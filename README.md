# language api for frisian

For english support we use stanford nlp for forms and the frisian english dictionary by Anne Dykstra.  
For Dutch support we use MOLEX by the Instituut Voor de Nederlandse Taal.

## changes in version 5
Version 5 introduces some breaking changes that makes processing linguistics much easier.  
- You don't have to process groups of GramType anymore, instead you just loop over FormInfo objects, each representing a linguistic category (pres_1_sing) and holding Paradigm forms belonging to it. 
- A Pos enum is introduced, simply allowing i.e. "verb" for a pos field or argument.

### Implementing the changes
- remove `pos_` from values wherever you use it
- adapt your queries, see examples in [model/src/test/resources](model/src/test/resources)
- if you process `Paradigm` with linguistics, refactor this into processing `FormInfo` holding `Paradigm`

### the changes
  - enum `GramType` removed
  - introduced enum `Pos`
  - replaced field `grammar` by `pos` of type `Pos` in `MinLemma`, `Lemma` and `LemmaLink`
  - removed field `subForms` from `MinLemma`
    - `subForms` are available in `Details` only
  - removed `ParadigmCategory` and `CategoryType`
    - removed `Paradigm` and `ParadigmCategory` from `SubForm`
  - removed field `grammar` from `Paradigm`
  - introduced type `FormInfo` holding `Paradigm`
    - `Paradigm` objects grouped in `FormInfo` and ordered logically
  - introduced enum `Form` for linguistics
  - added `FormInfo` to union `SubForm`