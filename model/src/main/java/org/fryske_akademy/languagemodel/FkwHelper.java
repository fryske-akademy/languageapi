package org.fryske_akademy.languagemodel;

/*-
 * #%L
 * languagemodel
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fa.foarkarswurdlist.ejb.IpaResults;
import org.fa.foarkarswurdlist.jpa.jsonb.Lemma;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.Util;

import java.util.Comparator;
import java.util.List;

public interface FkwHelper {
        
    int MAXLEMMAS = 3;

    Comparator<Paradigm> PARADIGM_COMPARATOR = Comparator
            .comparing(Paradigm::getPreferred, (s,s1) -> s.equals(s1) ? 0 : s ? -1 : 1)
            .thenComparing(Paradigm::getSplitForm, (s,s1) -> s.equals(s1) ? 0 : s1 ? -1 : 1);
//- paradigm: onfw, fkw
//- synonyms: fkw
//- variants: fkw, fhwb
//- dutchisms: fkw
//- hyphenation: fkw

    /**
     * convert Pos argument to pos if it's name starts with "pos_"
     * @param pos
     * @return pos or null
     */
    static Pc.Pos convert(Pos pos) {
        return pos==null||pos==Pos.abbr?null:Pc.Pos.valueOf(pos.name());
    }

    /**
     * convert a pos string in the form x.y to a Pos
     * @param pos
     * @return Pos or null
     */
    static Pos convert(String pos) {
        if (pos!=null&&pos.toLowerCase().startsWith("abbr.")) return Pos.abbr;
        return pos==null?Pos.x:Pos.valueOf(pos.toLowerCase().substring(pos.indexOf('.')+1));
    }

    List<FormInfo> getParadigm(String form, Pos pos);
    List<FormInfo> getFormInfos(List<LemmaParadigm> lemmaParadigms);

    static List<Paradigm> mapParadigm(org.fa.foarkarswurdlist.jpa.jsonb.Paradigm p, boolean noSplit) {
        String splitForm = noSplit?null:p.getSplitfoarm();
        if (splitForm!=null&&!splitForm.equals(p.getForm())) {
            return List.of(Paradigm.builder()
                            .setForm(splitForm)
                            .setSplitForm(true)
                            .setLang(LangType.fry)
                            .setPreferred(p.getPreferred())
                            .build(),
                    Paradigm.builder()
                            .setForm(p.getForm())
                            .setSplitForm(false)
                            .setPronunciation(p.getIpa())
                            .setLang(LangType.fry).setHyphenation(p.getHyphenation())
                            .setPreferred(p.getPreferred())
                            .build());
        }        return List.of(Paradigm.builder()
                .setForm(p.getForm())
                        .setSplitForm(false)
                .setLang(LangType.fry)
                .setHyphenation(p.getHyphenation())
                .setPronunciation(p.getIpa())
                .setPreferred(p.getPreferred())
                .build());
    }

    List<Synonym> getSynonyms(String form, Pos pos);

    static Synonym mapSynonym(org.fa.foarkarswurdlist.jpa.jsonb.Synonym s) {
        return Synonym.builder()
                .setForm(s.getForm()).setLang(LangType.fry)
                .setMeaning(Util.nullOrEmpty(s.getMeaning())?null: s.getMeaning())
                .build();
    }

    List<Variant> getVariants(String form, Pos pos);

    static Variant mapVariant(org.fa.foarkarswurdlist.jpa.jsonb.Variant s) {
        return Variant.builder().setForm(s.getForm()).setLang(LangType.fry).build();
    }

    List<Dutchism> getDutchisms(String form, Pos pos);

    static Dutchism mapDutchism(org.fa.foarkarswurdlist.jpa.jsonb.Dutchism s) {
        return Dutchism.builder().setForm(s.getForm()).setLang(LangType.nl).build();
    }

    String getHyphenation(String form);
    String getPronunciation(String form);
    String getArticle(String form);
    List<Lemma> findLemma(String form, Pos pos, int max, boolean extendedLemmaSearch);
    List<String> toIpa(String form, String pos, String lemma);
    IpaResults findFormsByIpa(int offset, int max, String... ipas);
    List<String> index(String lemma);

}
