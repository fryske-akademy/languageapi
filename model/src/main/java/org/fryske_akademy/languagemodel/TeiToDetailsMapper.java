package org.fryske_akademy.languagemodel;

/*-
 * #%L
 * languagemodel
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.schema.DataFetchingEnvironment;
import org.fryske_akademy.teidictionaries.jaxb.TEI;

import java.util.List;

public interface TeiToDetailsMapper {

    List<Details> fromTei(TEI tei, Pos gramType, String sourceId, DataFetchingEnvironment environment);

    public static class LemmaId {
        private final String lemma, id;

        public LemmaId(String lemma, String id) {
            this.lemma = lemma;
            this.id = id;
        }

        public String getLemma() {
            return lemma;
        }

        public String getId() {
            return id;
        }
    }

    LemmaId fromString(String lemmaRef);

    /**
     * return a list of {@link SubForm}s for lemma and pos, use for Frisian only
     * @param fkwHelper
     * @param lemma
     * @param pos part of speech or null
     * @param subForms which subform classes to fetch or null for all classes
     * @return
     */
    List<SubForm> getSubForms(FkwHelper fkwHelper, Lemma lemma, Pos pos, List<Class<? extends SubForm>> subForms);

}
