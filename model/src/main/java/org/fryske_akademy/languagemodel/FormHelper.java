package org.fryske_akademy.languagemodel;

/*-
 * #%L
 * languagemodel
 * %%
 * Copyright (C) 2021 - 2024 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fa.foarkarswurdlist.jpa.jsonb.Paradigm;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class FormHelper {
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    private static boolean has(List<String> gramTypes, Form requested) {
        return has(gramTypes, requested.name().split("_"));
    }

    private static boolean has(Pos pos, List<String> gramTypes, String... features) {
        return has(gramTypes,features) && gramTypes.get(gramTypes.size() - 1).contains(pos.name());
    }

        private static boolean has(List<String> gramTypes, String... features) {
        return gramTypes.stream().anyMatch(s -> Arrays.stream(features).allMatch(s::contains));
    }

    private static boolean is(Pos pos, Pos... posses) {
        return Arrays.stream(posses).anyMatch(p -> p == pos);
    }

    private static boolean is(Form form, Form... forms) {
        return Arrays.stream(forms).anyMatch(f -> f == form);
    }

    public static boolean isLinguisticsValidForForm(Pos pos, List<String> linguistics, Form form) {
        return isLinguisticsValidForForm(pos,linguistics,form,false);
    }
    /**
     * Given a Form, check if a list of linguistics {@link Paradigm#getLinguistics()} is valid for it.
     *
     * @param pos
     * @param linguistics
     * @param form
     * @param lenient be lenient
     * @return
     */
    public static boolean isLinguisticsValidForForm(Pos pos, List<String> linguistics, Form form, boolean lenient) {
        return
                switch (form) {
                    case pres_1_sing -> has(linguistics,form) && (lenient||!has(linguistics,"polite.form"));
                    case pres_2_sing -> has(linguistics,form) && !has(linguistics,"clitic") && (lenient||!has(linguistics,"polite.form"));
                    case pres_clitic -> has(linguistics,"clitic","pres");
                    case pres_3_sing -> has(linguistics,form);
                    case pres_polite -> has(linguistics,"polite.form","pres") && (lenient||!has(linguistics,"verbform.ger"));
                    case past_1_sing -> has(linguistics,form);
                    case past_2_sing -> has(linguistics,form) && !has(linguistics,"clitic");
                    case past_clitic -> has(linguistics,"clitic","past");
                    case past_3_sing -> has(linguistics,form);
                    case past_polite -> has(linguistics,"polite.form","past");
                    case pres_1_plur -> has(linguistics,form) && (lenient||!has(linguistics,"verbform.ger"));
                    case pres_2_plur -> has(linguistics,form) && (lenient||!has(linguistics,"verbform.ger"));
                    case pres_3_plur -> has(linguistics,form) && (lenient||!has(linguistics,"verbform.ger"));
                    case past_1_plur -> has(linguistics,form);
                    case past_2_plur -> has(linguistics,form);
                    case past_3_plur -> has(linguistics,form);
                    case pres_part -> has(linguistics,"verbform.part","pres", "uninf");
                    case pres_part_infl -> has(linguistics,"verbform.part","pres", "inflection.infl");
                    case past_part -> has(linguistics,"verbform.part","past") && !has(linguistics,"inflection.infl")
                            && (lenient||!has(linguistics,"verbform.ger"));
                    case verb_noun -> has(linguistics,"verbform.ger");
                    case sing -> has(Pos.noun,linguistics,"sing") && !has(linguistics,"dim") && !has(linguistics,"pos.num");
                    case plur -> has(Pos.noun,linguistics,"plur") && !has(linguistics,"dim");
                    case plur_tantum -> has(linguistics,"ptan");
                    case sing_tantum -> has(linguistics,"number.coll");
                    case dim_sing -> has(linguistics,"sing","dim");
                    case dim_plur -> has(linguistics,"plur","dim");
                    case pos_uninfl -> has(Pos.adj, linguistics, "uninf", "pred") && isAdjPos(linguistics);
                    case pos_infl -> has(Pos.adj, linguistics, "inflection.infl","noun") && isAdjPos(linguistics);
                    case cmp_uninfl -> has(linguistics,"degree.cmp","uninf", "pred");
                    case cmp_infl -> has(linguistics,"degree.cmp","inflection.infl","attr");
                    case sup_uninfl -> has(linguistics,"degree.sup","uninf", "attr");
                    case sup_infl -> has(linguistics,"degree.sup","inflection.infl", "noun");
                    case par_pos -> has(linguistics,"case.par") && isAdjPos(linguistics);
                    case par_cmp -> has(linguistics,"case.par","degree.cmp");
                    case par_pos_plur -> has(linguistics,"noun","plur") && isAdjPos(linguistics);
                    case par_cmp_plur -> has(linguistics,"noun","plur","degree.cmp");
                    case par_sup_plur -> has(linguistics,"noun","plur","degree.sup");
                    case num_card -> has(Pos.num,linguistics,"card", "sing") || has(Pos.num,linguistics,"card", "conv");
                    case num_card_plur -> has(Pos.num,linguistics,"card", "plur");
                    case num_ord -> has(Pos.num,linguistics,"ord", "sing") || has(Pos.num,linguistics,"ord", "conv");
                    case num_ord_plur -> has(Pos.num,linguistics,"ord", "plur");
                    case past_drop, pres_drop, unsupported, inf, base -> false;
                };
    }

    private static boolean isAdjPos(List<String> linguistics) {
        return !has(linguistics, "pos.num") && !has(linguistics, "degree.sup") && !has(linguistics, "degree.cmp");
    }

    public static final Pos detectPos(Form... forms) {
        return Arrays.stream(forms).map(f ->
                switch (f) {
                    case pres_part, pres_1_plur, pres_1_sing, pres_2_plur, pres_2_sing, pres_3_plur, pres_3_sing,
                         pres_clitic, pres_drop, pres_part_infl, pres_polite,
                         past_1_plur, past_1_sing, past_2_plur, past_2_sing, past_3_plur, past_3_sing, past_clitic,
                         past_drop, past_part, past_polite,
                         verb_noun, inf -> Pos.verb;
                    case sing, plur, sing_tantum, plur_tantum, dim_plur, dim_sing -> Pos.noun;
                    case pos_infl, pos_uninfl, cmp_infl, cmp_uninfl, sup_infl, sup_uninfl, par_pos, par_cmp,
                         par_cmp_plur, par_sup_plur, par_pos_plur -> Pos.adj;
                    case num_card, num_ord, num_card_plur, num_ord_plur -> Pos.num;
                    case base, unsupported -> Pos.x;
                }).findFirst().orElse(Pos.x);
    }

    public static final String describe(Form form) {
        return
                switch (form) {
                    case pres_1_sing -> "first person singular present tense";
                    case pres_2_sing -> "second person singular present tense";
                    case pres_drop -> "pronoun drop present tense";
                    case pres_clitic -> "clitic present tense";
                    case pres_3_sing -> "third person singular present tense";
                    case pres_polite -> "polite present tense";
                    case past_1_sing -> "first person singular past tense";
                    case past_2_sing -> "second person singular past tense";
                    case past_drop -> "pronoun drop past tense";
                    case past_clitic -> "clitic past tense";
                    case past_3_sing -> "third person singular past tense";
                    case past_polite -> "polite past tense";
                    case pres_1_plur -> "first person plural present tense";
                    case pres_2_plur -> "second person plural present tense";
                    case pres_3_plur -> "third person plural present tense";
                    case past_1_plur -> "first person plural past tense";
                    case past_2_plur -> "second person plural past tense";
                    case past_3_plur -> "third person plural past tense";
                    case pres_part -> "present particle";
                    case pres_part_infl -> "present particle inflected";
                    case past_part -> "past particle";
                    case verb_noun -> "verb used as noun";
                    case inf -> "infinitive";
                    case sing -> "singular";
                    case plur -> "plural";
                    case plur_tantum -> "only plural";
                    case sing_tantum -> "only singular";
                    case dim_sing -> "diminutive singular";
                    case dim_plur -> "diminutive plural";
                    case pos_uninfl -> "positive degree";
                    case pos_infl -> "positive degree inflected";
                    case cmp_uninfl -> "comparative degree";
                    case cmp_infl -> "comparative degree inflected";
                    case sup_uninfl -> "superlative degree";
                    case sup_infl -> "superlative degree inflected";
                    case par_pos -> "positive degree singular noun";
                    case par_cmp -> "comparative degree singular noun";
                    case par_pos_plur -> "positive degree plural noun";
                    case par_cmp_plur -> "comparative degree plural noun";
                    case par_sup_plur -> "superlative degree plural noun";
                    case num_card -> "numeric cardinal";
                    case num_ord -> "numeric ordinal";
                    case num_card_plur -> "numeric cardinal plural";
                    case num_ord_plur -> "numeric ordinal plural";
                    case unsupported -> "unsupported linguistics";
                    case base -> "base or lemma form";
                };
    }

}
