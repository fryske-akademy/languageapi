"""Language api for Dutch - Frisian - English, authoritative for Frisian, hence Frisian will show more details.
You can use english when searching in lemmas or texts (examples etc.), you can directly query the frisian-english source,
you can request details from the english source (i.e. using the links in results) and you can request
english translations for lemmas in details and in lemma results.
"""
type Query {
    "search for lemmas (lucene syntax), return a paginated graph with lemmas"
    lemmasearch(offset: Int!=0 max: Int!=10 searchterm: String! lang: LangType! pos: Pos
        "search in specific source, see sources query"
        source: String
        "search case and diacrit sensitive"
        sensitive: Boolean!=false
        "when true and language is frisian use lexicon results when dictionaries don't return any"
        lexiconFallback: Boolean!=false
        "when true english translations are shown next to frisian/dutch translations"
        englishTranslations: Boolean!=false
    ): Lemmas
    "search in text (lucene syntax), return a paginated graph with texts"
    textsearch(offset: Int!=0 max: Int!=10 searchterm: String! lang: LangType!
        "search in specific source, see sources query"
        source: String
        "search case and diacrit sensitive"
        sensitive: Boolean!=false): Texts
    "show detailed information of a lemma"
    details("case and diacrit sensitive lemma (article) to find" lemma: String!
        "part of speech for the lemma, see enum Pos" pos: Pos
        "details in specific source, see LemmaLink and sources query" source: String!
        "when true english translations for frisian results are shown together with dutch translations"
        englishTranslations: Boolean!=false
    ): [Details]
    "show lemmas around a given lemma in the frisian index, or the complete index"
    index(lemma: String): [String]!
    """show categorized lists of rhyming words, best rhyming on top."""
    rhyme(offset: Int!=0 max: Int!=500 word: String!
        "provide pos for better determination of ipa for input"
        pos: Pos
        "filter results on pos"
        posFilter: Pos
        "show less relevant results"
        showLessRelevant: Boolean!=false
        "When searchIpa is true the input is assumed to be ipa"
        searchIpa: Boolean!=false
        "should ipa be shown in the results"
        showIpa: Boolean!=false
        "how many syllables at most should be considered when looking for rhyming words"
        numSyllables: Int
        "should less common words be included"
        showLessCommon: Boolean!=false
        "include half rhyme"
        showHalfRhyme: Boolean!=false
        "should variants words be included"
        showVariants: Boolean!=false
    ): RhymeResults!
    "translate text"
    translatetext(text: String! lang: LangType!=nl): TextTranslation
    "request information of sources that can be searched"
    sources: [Source!]!
}

type RhymeResults implements PagingInterface {
    offset: Int!
    total: Int!
    max: Int!
    "total after filtering, for use in gui's"
    filteredTotal: Int!
    "number of syllable of the input word"
    syllableCount: Int!
    searchTerm: String!
    "the (calculated) phonetics for the input word, including similar sounds"
    ipa: [String!]!
    "the phonetics of the input word"
    totalIpa: String!
    rhymeGroups: [RhymeGroup]!
}

type RhymeGroup {
    "for less relevant and half rhyme no syllable count"
    syllableCount: Int
    description: String!
    rhyming: [Rhyming]!
    lessRelevant: Boolean
}

type Rhyming {
    text: String!
    ipa: String!
    "ipa with groups of characters marked between \">\" and \"<\", more of these means more similar to the input word"
    ipaMarked: String
    pos: Pos
}
type Source {
    "the id of the source as can be used in queries"
    source: String!
    "The language of lemmas in the source"
    fromlang: LangType!
    "The language of translations of lemmas in the source"
    tolang: LangType!
    title: String!
    "is this source a lexicon (with lemmas but without texts)"
    lexicon: Boolean!
}
"contract for paginated results"
interface PagingInterface {
    offset: Int!
    total: Int!
    max: Int!
}
"contract of a form"
interface FormInterface {
    form: String!
    lang: LangType!
}
"paginated lemma information"
type Lemmas implements PagingInterface {
    offset: Int!
    total: Int!
    max: Int!
    lemmas: [MinLemma]
}
type FormTranslation implements FormInterface {
    form: String!
    lang: LangType!
}
"minimal lemma information with translations for lemmasearch query"
type MinLemma implements FormInterface {
    form: String!
    lang: LangType!
    "Information to link to details of the lemma"
    link: LemmaLink!
    pos: Pos!
    "translations for the lemma, frisian lemmas may have both dutch and english translations"
    translations: [FormTranslation]
}
"""
extended lemma information, for details query.
"""
type Lemma implements FormInterface {
    form: String!
    pos: Pos!
    lang: LangType!
    note: Note
    meaning: String
    article: String
    hyphenation: String
    pronunciation: String
    "where lies the emphasis in pronunciation"
    stress: String
    usage: [Usg]
    "type for proper nouns"
    namekind: NameType
    subForms: [SubForm]
}

union SubForm = FormInfo|Synonym|Variant|Dutchism

type Paradigm implements FormInterface {
    form: String!
    "is this a split/broken form"
    splitForm: Boolean
    lang: LangType!
    note: Note
    hyphenation: String
    preferred: Boolean
    pronunciation: String
    "where lies the emphasis in pronunciation"
    stress: String
}
"grammar information with forms, logically ordered"
type FormInfo {
    linguistics: Form!
    description: String!
    paradigms: [Paradigm]
}

enum Form {
    "rinne, rinnen" inf
    "rin" pres_1_sing "rinst" pres_2_sing "rinst" pres_drop "rinsto" pres_clitic "rint" pres_3_sing "rinne" pres_polite "rinne" pres_1_plur "rinne" pres_2_plur "rinne" pres_3_plur
    "rûn" past_1_sing "rûnst" past_2_sing "rûnst" past_drop "rûnsto" past_clitic "rûn" past_3_sing "rûnen" past_polite "rûnen" past_1_plur "rûnen" past_2_plur "rûnen" past_3_plur "rûn"
    past_part "rinnend" pres_part "rinnende" pres_part_infl
    "rinnen" verb_noun
    sing plur dim_sing dim_plur plur_tantum sing_tantum
    "read" pos_uninfl "reader" cmp_uninfl "readst" sup_uninfl
    "reade" pos_infl "readere" cmp_infl "readste" sup_infl
    "reads" par_pos "readers" par_cmp
    "readen" par_pos_plur "readeren" par_cmp_plur "readsten" par_sup_plur
    "twa" num_card "twadde twads" num_ord
    "twaen" num_card_plur "twadden" num_ord_plur
    "unsupported form" unsupported
    "base or lemma form" base
}
"variant of a lemma"
type Variant implements FormInterface {
    form: String!
    lang: LangType!
}
"synonym for a lemma"
type Synonym implements FormInterface {
    form: String!
    lang: LangType!
    meaning: String
}
"dutchism for a lemma"
type Dutchism implements FormInterface {
    form: String!
    lang: LangType!
}
"paginated text information"
type Texts implements PagingInterface {
    offset: Int!
    total: Int!
    max: Int!
    texts: [Text]
}

interface TextInterface {
    "may be referred to from LemmaLink"
    id: String
    text: FormattedText!
    lang: LangType!
    note: Note
    usage: [Usg]
}

type Usg {
    type: UsgType!
    text: String!
}
"translation for example, proverb or collocation"
type TextTranslated implements TextInterface {
    id: String
    text: FormattedText!
    lang: LangType!
    note: Note
    usage: [Usg]
}
type Example implements TextInterface {
    id: String
    text: FormattedText!
    lang: LangType!
    translations: [TextTranslated]
    "only returned in details query"
    note: Note
    "only returned in details query"
    usage: [Usg]
    "Information for linking to details where the text can be found"
    link: LemmaLink!
}
type Collocation implements TextInterface {
    id: String
    text: FormattedText!
    "only returned in details query"
    definition: Definition
    "only returned in details query"
    note: Note
    lang: LangType!
    """Collocation may contain senses, this excludes translations, examples and usage.
    Only returned in details query
    """
    senses: [Sense]
    translations: [TextTranslated]
    "only returned in details query"
    examples: [Example]
    "only returned in details query"
    usage: [Usg]
    "Information for linking to details where the text can be found"
    link: LemmaLink!
}
type Proverb implements TextInterface {
    id: String
    text: FormattedText!
    "only returned in details query"
    definition: Definition
    lang: LangType!
    translations: [TextTranslated]
    "only returned in details query"
    note: Note
    "only returned in details query"
    usage: [Usg]
    "Information for linking to details where the text can be found"
    link: LemmaLink!
}

union Text = Example|Collocation|Proverb

"translation of text"
type TextTranslation {
    translation: String!
    translationlang: LangType
}

"details of a lemma"
type Details {
    lemma: Lemma!
    "translations for the lemma, frisian lemmas may have both dutch and english translations"
    translations: [Lemma]
    link: LemmaLink
    senses: [Sense]
    texts: [Text]
    "lemmas that refer to this lemma"
    referrers: [LemmaLink]
    "id of the data source"
    source: String!
}

union LemmaOrText = Lemma|TextTranslated

type Sense {
    "may be referred to from LemmaLink"
    id: String
    "A sense may use a different article"
    article: String
    "A note for this and following senses (that don't have a groupNote)"
    groupNote: Note
    definition: Definition
    "A Sense under Details holds a Lemma, a Sense under Collocation holds a TextTranslated"
    translations: [LemmaOrText]
    link: [LemmaLink]
    notes: [Note]
    texts: [Text]
}
"Definition holding at least text or a gloss"
type Definition {
    def: [DefContent!]!
    usage: [Usg]
}
union DefContent = FormattedText|Gloss
"Explanations for a definition"
type Gloss {
    gloss: FormattedText
}
type FormattedText {
    "all text objects in order, their types can be used to apply styling"
    text: [TextType!]!
}
"T, Q, I or L, Q can contain I and vise versa (max one level deep)"
union TextType = T|Q|I|L
"T or Q"
union TextTypeI = T|Q
"T or I"
union TextTypeQ = T|I

type L {
    link: LemmaLink!
}

"quoted / bracketed text"
type Q {
    "max one level of nesting"
    textQ: [TextTypeQ!]!
}
"emphasized / italic text"
type I {
    "max one level of nesting"
    textI: [TextTypeI!]!
}
"plain text"
type T {
    textT: String!
}
type Note {
    text: FormattedText
}
"""
Information for linking. Use lemma and lang for a lemmasearch; use source, lemma and pos to find details. Note that lang may differ
from lang in results.
"""
type LemmaLink {
    "the id of the source to link to"
    source: String
    "the lemma argument for a details query"
    lemma: String!
    "the language of the source/lemma, which may differ from the language of results"
    lang: LangType
    "the part of speech argument for a details query"
    pos: Pos
    "the id of a Sense or Text as found in the result of a details query"
    id: String
    "suggested text to show for a link"
    text: String
}
"possible values for iso language codes in this schema"
enum LangType {
    nl fry en
}
"possible values for usage information"
enum UsgType {
    lang time freq connotation hint style geo medium domain concerning
}
"possible values for details of proper nouns"
enum NameType {
    countryName placeName geoName orgName personName animalName plantName birdName
}

enum Pos {
    "Adjectives are words that typically modify nouns and specify their properties or attributes." adj
    "Adposition is a cover term for prepositions and postpositions." adp
    "Adverbs are words that typically modify verbs for such categories as time, place, direction or manner." adv
    "An auxiliary is a function word that accompanies the lexical verb of a verb phrase and expresses grammatical distinctions not carried by the lexical verb, such as person, number, tense, mood, aspect, voice or evidentiality." aux
    "A coordinating conjunction is a word that links words or larger constituents without syntactically subordinating one to the other and expresses a semantic relationship between them." cconj
    "Determiners are words that modify nouns or noun phrases and express the reference of the noun phrase in context." det
    "An interjection is a word that is used most often as an exclamation or part of an exclamation." intj
    "Nouns are a part of speech typically denoting a person, place, thing, animal or idea." noun
    "A numeral is a word, functioning most typically as a determiner, adjective or pronoun, that expresses a number and a relation to the number, such as quantity, sequence, frequency or fraction." num
    "Particles are function words that must be associated with another word or phrase to impart meaning and that do not satisfy definitions of other universal parts of speech." part
    "Pronouns are words that substitute for nouns or noun phrases, whose meaning is recoverable from the linguistic or extralinguistic context." pron
    "A proper noun is a noun (or nominal content word) that is the name (or part of the name) of a specific individual, place, or object." propn
    "Punctuation marks are non-alphabetical characters and character groups used in many languages to delimit linguistic units in printed text." punct
    "Not in universaldependencies. A conjunction is a conjunction that links constructions, where no assumption about the role of the constructions is made." conj
    "A subordinating conjunction is a conjunction that links constructions by making one of them a constituent of the other." sconj
    "A symbol is a word-like entity that differs from ordinary words by form, function, or both." sym
    "A verb is a member of the syntactic class of words that typically signal events and actions." verb
    "The tag X is used for words that for some reason cannot be assigned a real part-of-speech category." x
    "An abbreviation" abbr
}