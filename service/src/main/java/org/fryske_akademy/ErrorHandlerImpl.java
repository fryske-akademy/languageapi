package org.fryske_akademy;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.mail.Message;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Properties;

@ApplicationScoped
public class ErrorHandlerImpl implements ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandlerImpl.class);

    @Inject
    @Property
    private String smtp;
    @Inject
    @Property
    private String from;
    @Inject
    @Property
    private String to;
    @Inject
    @Property(defaultValue = "false")
    private boolean mailErrors;
    
    @Inject @Property(defaultValue = "true")
    private boolean test;

    @Override
    public void handle(String message, Throwable throwable) {
        String t = test ? "TEST " : "";
        if (mailErrors)
            mail(t+message,throwable);
        else
            LOGGER.warn(t+message,throwable);
    }

    private void mail(String message, Throwable throwable) {
        try (
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw)
        ) {
            if (throwable!=null) Util.deepestCause(throwable).printStackTrace(pw);
            pw.close();

            Properties props = new Properties();
            props.put("mail.smtp.host", smtp);

            try {
                Session session = Session.getInstance(props);
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom(from);
                msg.setRecipients(Message.RecipientType.TO, to);
                msg.setSubject("language api fault");
                msg.setSentDate(new Date());
                msg.setText(message + "\n\n" + sw);
                Transport.send(msg);
            } catch (Exception mex) {
                LOGGER.error("email fault: ",mex);
                LOGGER.error("original fault: " + message + "\n\n" + sw, throwable);
            }
        } catch (IOException e) {
            LOGGER.error("unexpected fault",e);
        }

    }

    @Override
    public boolean isTest() {
        return test;
    }
    
    

}
