package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.fa.foarkarswurdlist.ejb.IpaResults;
import org.fa.foarkarswurdlist.ejb.IpaService;
import org.fa.foarkarswurdlist.ejb.JsonbService;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;
import org.fa.foarkarswurdlist.jpa.jsonb.Lemma;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm;
import org.fa.foarkarswurdlist.jpa.jsonb.Paradigm;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.Util;
import org.fryske_akademy.languagemodel.Dutchism;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.Form;
import org.fryske_akademy.languagemodel.FormHelper;
import org.fryske_akademy.languagemodel.FormInfo;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.Synonym;
import org.fryske_akademy.languagemodel.Variant;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@ApplicationScoped
public class FkwHelperImpl implements FkwHelper {

    @Inject
    private JsonbService jsonbService;

    @Inject
    @StdwCrudBean
    private IpaService ipaService;

    @Override
    public List<FormInfo> getParadigm(String form, Pos pos) {
        List<LemmaParadigm> paradigms = jsonbService.findParadigm(form, FkwHelper.convert(pos), true);
        if (paradigms.isEmpty()) return Collections.emptyList();
        return getFormInfos(paradigms);
    }

    @Override
    public List<FormInfo> getFormInfos(List<LemmaParadigm> lemmaParadigms) {
        return Arrays.stream(Form.values())
                .map(f ->
                        FormInfo.builder()
                                .setLinguistics(f)
                                .setDescription(FormHelper.describe(f))
                                .setParadigms(
                                        lemmaParadigms.stream().map(lp -> {
                                                    Pos pos = FkwHelper.convert(lp.getPos());
                                                    final List<Paradigm> paradigms = lp.getParadigms().stream()
                                                            .filter(p -> FormHelper.isLinguisticsValidForForm(pos, getLinguistics(p), f))
                                                            .toList();
                                                    if (paradigms.stream().noneMatch(Paradigm::getPreferred)) {
                                                        // do some less restrictive matching
                                                        return lp.getParadigms().stream()
                                                                .filter(p -> FormHelper.isLinguisticsValidForForm(pos, getLinguistics(p), f, true))
                                                                .toList();
                                                    } else {
                                                        return paradigms;
                                                    }
                                                })
                                                .flatMap(Collection::stream)
                                                .filter(FormHelper.distinctByKey(Paradigm::getForm))
                                                .map(p -> FkwHelper.mapParadigm(p, f == Form.verb_noun || f == Form.past_part))
                                                .flatMap(Collection::stream)
                                                .sorted(PARADIGM_COMPARATOR).toList()
                                ).build())
                .filter(fi -> !fi.getParadigms().isEmpty())
                .toList();
    }

    public List<String> getLinguistics(org.fa.foarkarswurdlist.jpa.jsonb.Paradigm p) {
        return List.of(p.getLinguistics().replace("[", "").replace("]", "").toLowerCase().split(","));
    }

    @Override
    public List<Synonym> getSynonyms(String form, Pos pos) {
        return jsonbService.findSynonyms(form, FkwHelper.convert(pos), true).stream().map(
                FkwHelper::mapSynonym
        ).toList();
    }

    @Override
    public List<Variant> getVariants(String form, Pos pos) {
        return jsonbService.findVariants(form, FkwHelper.convert(pos), true).stream().map(FkwHelper::mapVariant
        ).toList();
    }

    @Override
    public List<Dutchism> getDutchisms(String form, Pos pos) {
        return jsonbService.findDutchisms(form, FkwHelper.convert(pos), true).stream().map(
                FkwHelper::mapDutchism
        ).toList();
    }

    @Override
    public String getHyphenation(String form) {
        return jsonbService.getHyphenation(form, true);
    }

    @Override
    public String getPronunciation(String form) {
        return jsonbService.getIpa(form, true);
    }


    @Override
    public String getArticle(String form) {
        return jsonbService.getArticle(form, true);
    }

    @Override
    public List<Lemma> findLemma(String form, Pos pos, int max, boolean extendedLemmaSearch) {
        return jsonbService.findLemma(form, FkwHelper.convert(pos), false, max, extendedLemmaSearch);
    }

    @Override
    public List<String> toIpa(String form, String pos, String lemma) {
        return ipaService.toIpa(form, lemma, Util.nullOrEmpty(pos) ? Pc.Pos.x : Pc.Pos.valueOf(pos.substring(4)));
    }

    private String cleanIpa(String ipa) {
        return ipa.endsWith("tt") ? ipa.substring(0, ipa.length() - 1) : ipa;
    }

    @Override
    public IpaResults findFormsByIpa(int offset, int max, String... ipas) {
        List<String> toList = Arrays.stream(ipas).map(this::cleanIpa)
                .filter(ipa -> !ipa.isEmpty()).toList();
        return toList.isEmpty() ? new IpaResults(offset, max, 0, Collections.emptyList()) :
                jsonbService.findFormsByIpa(offset, max, toList.toArray(new String[toList.size()]));
    }

    @Override
    public List<String> index(String lemma) {
        return jsonbService.index(lemma);
    }

}
