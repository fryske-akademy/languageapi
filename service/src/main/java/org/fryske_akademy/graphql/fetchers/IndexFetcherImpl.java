package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 - 2024 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.IndexFetcher;

import java.util.List;

@ApplicationScoped
public class IndexFetcherImpl implements IndexFetcher {

    @Inject
    private FkwHelper fkwHelper;

    @Override
    public List<String> get(DataFetchingEnvironment environment) throws Exception {
        return fkwHelper.index(environment.getArgumentOrDefault("lemma",""));
    }
}
