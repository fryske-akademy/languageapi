package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.jayway.jsonpath.JsonPath;
import com.vectorprint.configuration.cdi.Property;
import graphql.schema.DataFetchingEnvironment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.JsonArray;
import jakarta.ws.rs.NotFoundException;
import org.fryske_akademy.Util;
import org.fryske_akademy.languagemodel.Details;
import org.fryske_akademy.languagemodel.DetailsFetcher;
import org.fryske_akademy.languagemodel.LemmaLink;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.SourcesFetcher;
import org.fryske_akademy.languagemodel.TeiToDetailsMapper;
import org.fryske_akademy.teidictionaries.jaxb.TEI;
import org.fryske_akademy.validation.FA_ValidationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ApplicationScoped
public class DetailsFetcherImpl extends AbstractFetcher<List<Details>> implements DetailsFetcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(DetailsFetcherImpl.class);
    public static final String XMLSOURCEPATH = "/xmlSource";
    public static final String REFERRERSPATH = "/referers";

    @Inject
    private TeiToDetailsMapper teiToDetailsMapper;

    @Inject
    @Property(defaultValue = "false")
    private boolean validateDetailXml;

    private HttpRequest detailRequest(DataFetchingEnvironment environment, String url, String term) {
        return headers(HttpRequest.newBuilder()
                .uri(URI.create(url))
                .POST(HttpRequest.BodyPublishers.ofString(
                        String.join("", LEMMA, "=",
                                URLEncoder.encode(term, StandardCharsets.UTF_8),
                                environment.getSelectionSet().contains("texts") ?
                                        "" : "&notext=true"
                        )
                )))
                .build();
    }

    private HttpRequest referrersRequest(String url, String term) {
        return headers(HttpRequest.newBuilder()
                .uri(URI.create(url))
                .POST(HttpRequest.BodyPublishers.ofString(
                        String.join("", LEMMA, "=",
                                URLEncoder.encode(term, StandardCharsets.UTF_8))
                )))
                .build();
    }

    @Override
    public List<Details> get(DataFetchingEnvironment environment) throws Exception {
        String xmlBytes = null, url, refurl, referrers = null;
        final String source = environment.getArgument("source");
        SearchInfo searchInfo = getSearchInfo(environment);
        if (!islexicon(environment)) {
            url = searchInfo.getUrls().get(0) + XMLSOURCEPATH;
            refurl = searchInfo.getUrls().get(0) + REFERRERSPATH;
            xmlBytes = getRequestHelper().request(detailRequest(environment, url, searchInfo.getQuery()), getTimeoutSeconds());
            if (environment.getSelectionSet().contains("referrers")) {
                referrers = getRequestHelper().request(referrersRequest(refurl, searchInfo.getQuery()), getTimeoutSeconds());
            }
        }

        if (!Util.nullOrEmpty(xmlBytes)) {

                String pos = environment.getArgumentOrDefault("pos", "");
                List<Details> details = teiToDetailsMapper.fromTei(FA_ValidationHelper.fromXML(
                                new StringReader(xmlBytes)
                                , TEI.class, validateDetailXml)
                        , pos == null || pos.isEmpty() ? null : Pos.valueOf(environment.getArgument("pos")),
                        source, environment);
                if (details == null || details.isEmpty()) {
                    throw new NotFoundException(String.format("%s, %s not found in dictionaries",
                            environment.getArgument("lemma"),
                            environment.getArgument("pos")));
                }
                if (!Util.nullOrEmpty(referrers)) {
                    Object refs = JsonPath.read(referrers, "$");
                    if (refs instanceof JsonArray ja) {
                        List<LemmaLink> links = new ArrayList<>();
                        ja.forEach(v -> {
                            TeiToDetailsMapper.LemmaId lemmaId = teiToDetailsMapper.fromString(v.toString());
                            links.add(LemmaLink.builder()
                                    .setId(lemmaId.getId())
                                    .setSource(source)
                                    .setLang(details.get(0).getLemma().getLang())
                                    .setLemma(lemmaId.getLemma())
                                    .setText(v.toString()).build());

                        });
                        if (!links.isEmpty()) {
                            details.forEach(d -> d.setReferrers(links));
                        }
                    } else {
                        details.forEach(d -> {
                            TeiToDetailsMapper.LemmaId lemmaId = teiToDetailsMapper.fromString(refs.toString());
                            d.setReferrers(
                                    Collections.singletonList(LemmaLink.builder()
                                            .setId(lemmaId.getId())
                                            .setSource(source)
                                            .setLang(d.getLemma().getLang())
                                            .setLemma(lemmaId.getLemma())
                                            .setText(refs.toString()).build())
                            );
                        });
                    }
                }
                return details;
        } else if (!searchInfo.getFkwLemmas().isEmpty()) {
            return searchInfo.getFkwLemmas().stream().map(
                    l -> Details.builder()
                            .setSource(SourcesFetcher.SOURCE.fkw.name())
                            .setLemma(l)
                            .build()
            ).toList();
        } else {
            throw new NotFoundException(String.format("%s, %s not found in dictionaries",
                    environment.getArgument("lemma"),
                    environment.getArgument("pos")));
        }
    }

}
