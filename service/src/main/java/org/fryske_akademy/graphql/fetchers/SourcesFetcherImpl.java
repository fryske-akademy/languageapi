package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.schema.DataFetchingEnvironment;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.Source;
import org.fryske_akademy.languagemodel.SourcesFetcher;

import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@ApplicationScoped
public class SourcesFetcherImpl extends AbstractFetcher<List<Source>> implements SourcesFetcher {

    private final List<Source> sources = new ArrayList<>();

    private static final Source FKW = Source.builder().setSource(SOURCE.fkw.name())
            .setFromlang(LangType.fry).setTolang(LangType.fry).setTitle("Frisian Lexicon").setLexicon(true).build();

    @PostConstruct
    private void init() {
        Arrays.stream(SOURCE.values()).filter(s -> s != SOURCE.fkw).map(source -> {
            JsonObject info = getInfo(getSourceUrl(source.name()));
            if (info != null) {
                return Source.builder()
                        .setSource(source.name())
                        .setFromlang(LangType.valueOf(info.getString("fromlang")))
                        .setTolang(LangType.valueOf(info.getString("tolang")))
                        .setTitle(info.getString("title"))
                        .build();
            } else {
                return null;
            }
        }
        ).filter(Objects::nonNull).forEach(sources::add);
        sources.add(FKW);
        Arrays.stream(SOURCE.values()).forEach(s -> {
                if (sources.stream().noneMatch(ss -> ss.getSource().equals(s.name()))) {
                    throw new IllegalStateException(s + " not initialized");
                }
            }
        );
    }

    private List<Source> getSources() {
        return sources;
    }

    private JsonObject getInfo(String u) {
        try {
            String output = getRequestHelper().request(HttpRequest.newBuilder()
                    .uri(URI.create(u + INFOTEXTPATH))
                    .GET()
                    .build(), getTimeoutSeconds());
            return Json.createReader(new StringReader(output)).readObject();
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            return null;
        }
    }

    @Override
    public List<Source> get(DataFetchingEnvironment environment) {
        return getSources();
    }

    @Override
    public Source getSource(String source) {
        if (Arrays.stream(SOURCE.values()).noneMatch(s -> s.name().equals(source))) {
            return null;
        }
        return sources.stream().filter(s -> s.getSource().equals(source)).findFirst().get();
    }
}
