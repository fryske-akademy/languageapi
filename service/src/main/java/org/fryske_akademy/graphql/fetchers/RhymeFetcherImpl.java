package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 - 2023 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import graphql.schema.DataFetchingEnvironment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.NotFoundException;
import org.fa.foarkarswurdlist.ejb.FormIpa;
import org.fa.foarkarswurdlist.ejb.JsonbService;
import org.fa.foarkarswurdlist.jpa.jsonb.Lemma;
import org.fa.foarkarswurdlist.jpa.jsonb.Paradigm;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.Util;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.RhymeFetcher;
import org.fryske_akademy.languagemodel.RhymeGroup;
import org.fryske_akademy.languagemodel.RhymeResults;
import org.fryske_akademy.languagemodel.Rhyming;
import org.fryske_akademy.languagemodel.SearchLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class RhymeFetcherImpl implements RhymeFetcher {
    
    @Inject
    private FkwHelper fkwHelper;
    /**
     * Vowels are defined as not consonants, in this pattern the - is counted as
     * consonant, some consonants consist out of more characters
     */
    private static final String VOWELPATTERN = "[^-" + JsonbService.IPCONSONANTS + "]";

    /**
     * Pattern that finds leading consonants, including some diacrits
     */
    private static final Pattern LEADINGCONSPATTERN = Pattern.compile("^[̃ː ̯̃" + JsonbService.IPCONSONANTS + "]*");
    /**
     * Pattern that finds groups of spaces
     */
    private static final Pattern SPACE = Pattern.compile(" +");

    /**
     * A list containing uncommon lemmas
     */
    private static final Set<String> COMMON;
    private static final Set<String> OLD;
    
    static {
        try (
                InputStream old = FkwHelper.class.getResourceAsStream("/old.txt")) {
            OLD = new BufferedReader(new InputStreamReader(old,
                    StandardCharsets.UTF_8)).lines().collect(Collectors.toSet());
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        try (
                InputStream common = FkwHelper.class.getResourceAsStream("/frekwinsjes.txt")) {
            COMMON = new BufferedReader(new InputStreamReader(common,
                    StandardCharsets.UTF_8)).lines().collect(Collectors.toSet());
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * The maximum length for input
     */
    @Inject
    @Property(defaultValue = "30")
    private int maxLength;
    /**
     * When the ipa for a word ends with one of these very frequent endings, use
     * one extra syllable to search rhyming words
     */
    @Inject
    @Property(defaultValue = {"ɪŋ", "əstoː", "ɛstoː", "stoː", "ɪŋŋ", "i"})
    private String[] extraSyllableForIpa;
    
    @Inject
    @Named("rhymeLogger")
    private SearchLogger searchLogger;
    
    private static final Map<String, String> similarIpa = Map.of(
            "o", "ɔ",
            "ɔ", "ɔː",
            "aˑi̯", "ɛˑi̯",
            "ɛː", "ɛ",
            "iː", "iˑə",
            "uː", "uˑə",
            "u̯a", "a",
            "u̯o", "o"
    );

    /**
     * combinations of vowels that consist of 2 syllables
     */
    private static final Map<String, Integer> twoSyllables
            = Stream.of(
                    Map.of(
                            "uaa", 1, "eeë", 2, "ieë", 2,
                            "ië", 1, "eü", 1, "uü", 1,
                            "oö", 1, "eö", 1, "eä", 1,
                            "oä", 1
                    ).entrySet().stream(),
                    Map.of(
                            "aaï", 2, "eiï", 2, "eï", 1,
                            "oï", 1, "uiï", 2,
                            "oaiï", 3, "oeiï", 3, "aaiï", 3,
                            "iï", 1, "aaie", 2
                    ).entrySet().stream(),
                    Map.of(
                            "oaie", 2, "uie", 1, "oeie", 2,
                            "aːi̯ə", 2, "oːi̯ə", 2, "uˑi̯ə", 2,
                            "œˑi̯ə", 2
                    ).entrySet().stream()
            ).flatMap(e -> e).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    /**
     * Pattern that finds groups of vowels
     */
    private static final Pattern VOWELREGEX = Pattern.compile(VOWELPATTERN + "+");
    /**
     * Pattern that matches a string that consists of only consonants
     */
    private static final Pattern ALLCONSREGEX = Pattern.compile("^[" + JsonbService.IPCONSONANTS + "]+$");
    
    private boolean invalidInput(String word) {
        
        return (!VOWELREGEX.matcher(word).find())
                || word.length() > maxLength
                || SPACE.matcher(word).results().count() > 1;
    }

    private String clip(String s, int max) {
        return s.length() > max ? s.substring(0, max) : s;
    }
    
    private <T> T argOrNull(DataFetchingEnvironment environment, String name) {
        return environment.containsArgument(name) ? environment.getArgument(name) : null;
    }
    
    @Override
    public RhymeResults get(DataFetchingEnvironment environment) throws Exception {
        String w = environment.getArgumentOrDefault("word", "").toLowerCase();
        searchLogger.log("Incomming rhyme request");
        if (invalidInput(w)) {
            throw new IllegalArgumentException(clip(w, maxLength) + " not accepted");
        }
        final String word = SPACE.matcher(w).replaceFirst("");
        String pos = argOrNull(environment, "pos");
        String lemma = null;
        boolean searchIpa = environment.getArgumentOrDefault("searchIpa", Boolean.FALSE);
        boolean showLessCommon = environment.getArgumentOrDefault("showLessCommon", Boolean.FALSE);
        boolean showVariants = environment.getArgumentOrDefault("showVariants", Boolean.FALSE);
        boolean showLessRelevant = environment.getArgumentOrDefault("showLessRelevant", Boolean.FALSE);
        boolean showHalfRhyme = environment.getArgumentOrDefault("showHalfRhyme", Boolean.FALSE);
        String pf = argOrNull(environment, "posFilter");
        final Pos posFilter = pf == null ? null : Pos.valueOf(pf);
        int offset = Math.max(environment.getArgumentOrDefault("offset", 0), 0);
        int max = environment.getArgumentOrDefault("max", 500);
        Integer numSyllables = argOrNull(environment, "numSyllables");
        String ti = null;

        List<Lemma> lemmas = fkwHelper.findLemma(word, null, 2, false);
        if (lemmas.size() == 1) {
            if (pos == null) {
                pos = lemmas.get(0).getPos().toLowerCase();
                if (pos.equalsIgnoreCase("abbr.yes")) {
                    throw new IllegalArgumentException("Cannot rhyme to abbreviation");
                }
            }
            lemma = lemmas.get(0).getForm().toLowerCase();
            if (!searchIpa) {
                ti = lemmas.get(0).getParadigms().stream()
                        .filter(p -> p.getForm().equalsIgnoreCase(word) || p.getNodiacrits().equalsIgnoreCase(word))
                        .findFirst().orElseGet(Paradigm::new).getIpa();
            }
        } else if (lemmas.isEmpty()) {
            searchLogger.log("NOT IN fkw: " + word);
        }
        
        String ipa = searchIpa ? word : "";
        String totalIpa = searchIpa ? ipa : Util.nullOrEmpty(ti) ? toIpa(word, pos, lemma) : ti;
        // holder for the calculated number of syllables
        final AtomicInteger calcSyl = new AtomicInteger(countSyllables(word));
        
        if (!searchIpa) {
            // contains syllables as found in fkw
            List<String> syllables = getFkwSyllables(word);

            List<VowelGroup> vowelGroupsWithPos = vowelGroupsWithPos(totalIpa);
            if (vowelGroupsWithPos.isEmpty()) {
                searchLogger.log(String.format("no vowels in ipa for %s (%s)", word, totalIpa));
                throw new IllegalArgumentException(String.format("no vowels in ipa for %s (%s)", word, totalIpa));
            }
            VowelGroup vowelGroup = vowelGroupsWithPos.get(vowelGroupsWithPos.size() - 1);
            int afterLastVG = totalIpa.length() - vowelGroup.end;
            List<String> vg = vowelGroupsWithPos.stream().map(v -> v.ipa).toList();
            boolean absentIpaVowels = afterLastVG > ABSENT_VOWEL_LAST_LIMIT && calcSyl.get() > vg.size();
            // number of syllables to consider, number as provided by the client takes precedence
            final int lastSyllables = numSyllables == null ? compareLastN(totalIpa, absentIpaVowels) : numSyllables;
            // always count syllables using logic
            int diff = calcSyl.get() - syllables.size();
            if (diff != 0 && !syllables.isEmpty()) {
                searchLogger.log(String.format("Wrong hyph for %s n fkw, should be %d syllables", word, calcSyl.get()));
                syllables = Collections.emptyList();
            } else if (absentIpaVowels && !syllables.isEmpty()) {
                searchLogger.log(String.format("number of syllables in ipa differs for %s", word));
                syllables = Collections.emptyList();
            }
            /*
             determine the graphemes to find phonemes for, when no syllables available from fkw or wrong
             try to determine the last syllables to consider using logic
             */
            String toSearch = syllables.isEmpty()
                    ? lastSyllables(word, lastSyllables, calcSyl.get())
                    : word;
            if (toSearch.isEmpty()) {
                throw new IllegalArgumentException("cannot rhyme to " + word);
            }
            ipa = findIpa(toSearch, pos, lemma, syllables, lastSyllables, calcSyl.get(), totalIpa);
            // when vowels absent, try one syllable more
            if (lastSyllables == 1 && syllables.isEmpty() && ALLCONSREGEX.matcher(ipa).matches()) {
                toSearch = lastSyllables(word, lastSyllables + 1, calcSyl.get());
                ipa = findIpa(toSearch, pos, lemma, syllables, lastSyllables, calcSyl.get(), totalIpa);
            }
        }
        if (ipa.isEmpty()) {
            throw new IllegalArgumentException("ipa empty, cannot rhyme to " + word);
        }
        List<String> similar = showHalfRhyme ? findSimilarSound(ipa) : List.of(ipa);
        final AtomicInteger filteredTotal = new AtomicInteger();
        var rhymeResults = fkwHelper.findFormsByIpa(offset, max, similar.toArray(new String[similar.size()]));
        List<RhymeGroup> rv = new ArrayList<>(rhymeResults.formIpas()).parallelStream().filter(
                i -> !i.getForm().equalsIgnoreCase(word)
                && !i.getForm().contains(" ")
                && (showLessCommon || !OLD.contains(i.getLemma()))
                && (showVariants || !i.isVariant())
                && (posFilter == null || i.getPos() == Pc.Pos.valueOf(posFilter.name().substring(4)))
        )
                .map(fi -> new FormIpaWrapper(
                fi,
                similarity(fi.getIpa(), vowelGroups(totalIpa), showHalfRhyme, new StringBuilder(fi.getIpa())),
                isHalfRhyme(fi, similar),
                vowelGroups(fi.getIpa())
        )
                )
                .sorted(FORM_IPA_WRAPPER_COMPARATOR)
                .distinct()
                // group by number of syllables, distinguishing two special groups: -1 = same number of syllables, less relevant = 500
                .collect(Collectors.groupingBy(fi -> countSyllablesForGrouping(fi, word, calcSyl.get()))).entrySet().stream()
                .filter(e -> showLessRelevant || e.getKey() != 500)
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .map(
                        e -> RhymeGroup.builder()
                                .setLessRelevant(e.getKey() == 500)
                                .setSyllableCount(e.getKey() == 500 ? null : e.getKey() == -1 ? calcSyl.get() : e.getKey())
                                .setDescription(description(e.getKey(), calcSyl.get()))
                                .setRhyming(e.getValue().stream()
                                        .filter(fi -> filteredTotal.get() < max)
                                        .map(
                                                fi -> {
                                                    Rhyming r = Rhyming.builder().setText(fi.getForm())
                                                            .setIpa(fi.getIpa())
                                                            .setPos(Pos.valueOf(fi.getPos().name()))
                                                            .setIpaMarked(fi.getSimilarity().getMarkedIpa())
                                                            .build();
                                                    filteredTotal.incrementAndGet();
                                                    return r;
                                                }
                                        ).toList()
                                ).build()
                ).toList();
        boolean searchMax = showLessCommon && showLessRelevant && showVariants && showHalfRhyme && numSyllables == null && pos == null;
        if (searchMax && rv.isEmpty()) {
            // only log empty when results are not limited
            searchLogger.log("Nothing found for " + w);
        }
        if (rv.isEmpty()) {
            throw new NotFoundException("Nothing found for " + w);
        }
        return RhymeResults.builder()
                        .setSearchTerm(w)
                        .setIpa(similar)
                        .setTotalIpa(totalIpa)
                        .setFilteredTotal(rv.stream().map(g -> g.getRhyming().size()).reduce(0, Integer::sum))
                        .setSyllableCount(calcSyl.get())
                        .setMax(rhymeResults.max())
                        .setOffset(rhymeResults.offset())
                        .setTotal(rhymeResults.total())
                        .setRhymeGroups(rv).build();
    }
    private static final int ABSENT_VOWEL_LAST_LIMIT = 1;

    /**
     * When the list of syllables isn't empty construct the string to find ipa
     * for from that, otherwise use the word argument
     *
     * @param word
     * @param syllables
     * @param lastSyl
     * @return
     */
    private String findIpa(String word, String pos, String lemma, List<String> syllables, int lastSyl, int numSyllables, String totalIpa) {
        String lastSyllables = word;
        int offset = numSyllables - lastSyl;
        if (!syllables.isEmpty() && offset >= 0 && offset < syllables.size()) {
            // concatenate last syllables from fkw
            lastSyllables = String.join("", syllables.subList(offset, syllables.size()));
        }
        String rv = toIpa(lastSyllables, pos, lemma);
        /*
        this solves difference in pronounciation when only last syllables are used to find ipa
        the assumption is that the length of the ipa for the last syllables equals the length
        of the ipa we need to use
        TODO keppelest => peːləst, kɛpələst and maybe more
        ferjitten => rjɪtn, fəjɪtn. Can be solved by checking length of parts without leading consonants
         */
        String partRv = LEADINGCONSPATTERN.matcher(rv).replaceFirst("");
        int extra = partRv.startsWith("eː") ? 1 : 0;
        String partTot = totalIpa.substring(Math.max(totalIpa.length() - rv.length(), 0));
        partTot = LEADINGCONSPATTERN.matcher(partTot).replaceFirst("");
        if (!partTot.startsWith("eː")&&extra==1) partTot=partTot.substring(extra);
        if (partRv.length()==partTot.length()) rv=partTot;
        // ɡljə
        if (ALLCONSREGEX.matcher(rv).matches()) {
            return rv;
        } else {
            // strip leading consonants, but only if the remaining length is big enough
            String stripped = LEADINGCONSPATTERN.matcher(rv).replaceFirst("");
            if (stripped.startsWith(W_IPA)) {
                stripped = stripped.substring(W_IPA.length());
            }
            return stripped.length() > 1 ? stripped : rv;
        }
    }
    
    private String description(int numSyl, int nSylInput) {
        return switch (numSyl) {
            case -1 ->
                "Same number of syllables (" + nSylInput + ")";
            case 500 ->
                "Less relevant";
            default ->
                numSyl + " syllable" + (numSyl > 1 ? "s" : "");
        };
    }

    /**
     * determine similarity based on the number of equal vowels in syllables
     * calculated from the end. The groups of characters in are marked between >
     * and &lt; for the purpose of highlighting
     *
     * @param ipa
     * @param inputVowels
     * @return
     */
    private Similarity similarity(String ipa, List<String> inputVowels, boolean showHalfRhyme, StringBuilder ipaMarked) {
        int i = 100;
        List<VowelGroup> ipaVowelsPos = vowelGroupsWithPos(ipa);
        
        List<String> ipaVowels = ipaVowelsPos.stream().map(v -> v.ipa).toList();
        
        List<String> biggest = ipaVowels.size() > inputVowels.size() ? ipaVowels : inputVowels;
        List<String> smallest = ipaVowels.size() > inputVowels.size() ? inputVowels : ipaVowels;
        boolean ipaBigger = ipaVowels.size() > inputVowels.size();
        
        int diff = biggest.size() - smallest.size();
        int start = biggest.size() - 1;
        
        for (int s = start; s - diff > -1; s--) {
            String v1 = biggest.get(s);
            String v2 = smallest.get(s - diff);
            if (v1.equals(v2)) {
                VowelGroup hit = ipaVowelsPos.get(ipaBigger ? s : s - diff);
                ipaMarked.insert(hit.start, ">");
                ipaMarked.insert(hit.end + 1, "<");
                i--;
            } else {
                break;
            }
        }
        return new Similarity(i, ipaMarked.toString());
    }
    
    private List<String> vowelGroups(final String ipaOrWord) {
        return vowelGroupsWithPos(ipaOrWord).stream().map(v -> v.ipa).toList();
    }

    /**
     * return vowelgroups with their positions in the input
     *
     * @param ipaOrWord ipa or word
     * @return
     */
    private List<VowelGroup> vowelGroupsWithPos(final String ipaOrWord) {
        List<VowelGroup> rv = new ArrayList<>();
        VOWELREGEX.matcher(ipaOrWord).results()
                .map(mr -> new VowelGroup(mr.start(), mr.end(), mr.group()))
                .forEach(vg -> {
                    String v = vg.ipa;
                    if (v.contains(W_IPA)) {
                        // omit ipa w
                        int start = v.indexOf(W_IPA);
                        if (start == 0) {
                            if (W_IPA.length() < v.length()) {
                                // part after W_IPA
                                rv.add(new VowelGroup(vg.start + W_IPA.length(), vg.end, v.substring(W_IPA.length())));
                            }
                        } else {
                            // part before W_IPA
                            rv.add(new VowelGroup(vg.start, vg.start + start, v.substring(0, start)));
                            if (start + W_IPA.length() < v.length()) {
                                // part after W_IPA
                                rv.add(new VowelGroup(vg.start + start + W_IPA.length(), vg.end, v.substring(start + W_IPA.length())));
                            }
                        }
                    } else {
                        if (twoSyllables.containsKey(v)) {
                            // first part
                            rv.add(new VowelGroup(vg.start, vg.start + twoSyllables.get(v), v.substring(0, twoSyllables.get(v))));
                            // second part
                            rv.add(new VowelGroup(vg.start + twoSyllables.get(v), vg.end, v.substring(twoSyllables.get(v))));
                        } else {
                            rv.add(vg);
                        }
                    }
                });
        return rv;
    }
    
    private static final String W_IPA = "u̯";

    /**
     * count the number of syllables of a result for purpose of grouping and
     * sorting those groups. When less relevant 500 is returned, when the number
     * of syllables equals the input -1 is returned
     *
     * @param fi
     * @param input
     * @param numInputSyllables
     * @return
     */
    private int countSyllablesForGrouping(FormIpaWrapper fi, String input, int numInputSyllables) {
        int toIntExact = countSyllables(fi.getForm());
        return lessRelevant(fi, input, numInputSyllables) ? 500 : numInputSyllables == toIntExact ? -1 : toIntExact;
    }

    /**
     * determine if a result is less relevant: when the result ends with the
     * input, when it is less common, aging or when it is a variant
     *
     * @param fi
     * @param input
     * @return
     */
    private boolean lessRelevant(FormIpaWrapper fi, String input, int numInputSyllables) {
        return numInputSyllables > 1 && fi.getForm()
                .toLowerCase().endsWith(input)
                || input.toLowerCase().endsWith(fi.getForm())
                || OLD.contains(fi.getLemma())
                || !(COMMON.contains(fi.getLemma()) || COMMON.contains(fi.getForm()))
                || fi.isVariant();
    }
    
    private boolean isHalfRhyme(FormIpa fi, List<String> similar) {
        return similar.stream().skip(1).anyMatch(s -> fi.getIpa().endsWith(s));
    }
    
    private static final Function<FormIpaWrapper, String> FormIpaToForm = FormIpa::getForm;
    private static final JsonbService.FrisianComparator<FormIpaWrapper> FRISIAN_COMPARATOR;
    
    static {
        try {
            FRISIAN_COMPARATOR = new JsonbService.FrisianComparator<>(
                    JsonbService.FRY_COLLATION,
                    FormIpaToForm
            );
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }
    }
    
    private final Comparator<FormIpaWrapper> FORM_IPA_WRAPPER_COMPARATOR
            = Comparator
                    .comparing(FormIpaWrapper::getHalfRhymeEnding)
                    .thenComparingInt(fiw -> fiw.getSimilarity().getWeight())
                    .thenComparing(FRISIAN_COMPARATOR);
    
    @Override
    public List<String> getFkwSyllables(String word) {
        String hyphenation = fkwHelper.getHyphenation(word);
        if (!Util.nullOrEmpty(hyphenation)) {
            return new Scanner(hyphenation).useDelimiter("[.]").tokens().toList();
        }
        searchLogger.log("no hyph for " + word);
        return Collections.emptyList();
    }

    /**
     * Attempt to find last syllables when no or wrong syllables found in fkw
     * taking the substring after the nth group of vowels found based on number
     * of last syllables to consider and the number of syllables of the input
     * word
     *
     * @param word
     * @param lastSyl
     * @param numSyl
     * @return
     */
    private String lastSyllables(String word, int lastSyl, int numSyl) {
        if (numSyl == 1 || numSyl <= lastSyl) {
            return word;
        }
        int vowelsToFind = numSyl - lastSyl;
        Matcher m = VOWELREGEX.matcher(word);
        for (short s = 0; s < vowelsToFind; s++) {
            m.find();
        }
        int matchEnd = m.end();

        /*
        return the part of the word from the start of the last match
        This works if the number of syllables to match is 2 or more, which is the case here
         */
        return word.substring(matchEnd);
    }
    
    private int countSyllables(String word) {
        return vowelGroups(word).size();
    }

    /**
     * Check if the ip argument ends with ipa to be ignored (configured with a
     * property "extraSyllableForIpa")
     *
     * @param ipa
     * @return
     */
    private Optional<String> ignoreEnding(String ipa) {
        return Arrays.stream(extraSyllableForIpa).filter(c -> ipa.endsWith(c))
                .findFirst();
    }

    /**
     * determine how many syllables should be compared when looking for rhyming
     * words. This will be 1 plus the number of ending syllables to be ignored.
     * Syllables to be ignored can be configured via "extraSyllableForIpa",
     * furthermore the last syllable will be ignored when it contains only a
     * sjwa
     *
     * @param absentIpaVowels when true assume last vowel is absent in ipa, thus
     * add 1 more syllable to compare
     * @param ipa
     * @return
     */
    private int compareLastN(String ipa, boolean absentIpaVowels) {
        Optional<String> ignore = ignoreEnding(ipa);
        int toIgnore = ignore.map(this::countSyllables).orElseGet(() -> endSjwa(ipa) ? 1 : 0);
        return absentIpaVowels ? 2 + toIgnore : 1 + toIgnore;
    }
    
    private boolean endSjwa(String ipa) {
        List<String> vowels = vowelGroups(ipa);
        return !vowels.isEmpty() && (vowels.get(vowels.size() - 1).equals("ə")
                || vowels.get(vowels.size() - 1).equals("i̯ə"));
    }
    
    @Override
    public int getStress(String word) {
        return 0;
    }
    
    @Override
    public String toIpa(String word, String pos, String lemma) {
        return fkwHelper.toIpa(word, pos, lemma).stream().findFirst().orElse("");
    }

    /**
     * replace max two occurrences of sound1 with sound2 in ipa
     *
     * @param ipa
     * @param sound1
     * @param sound2
     * @return
     */
    private String otherIpa(String ipa, String sound1, String sound2) {
        String out;
        int start = ipa.indexOf(sound1);
        if (isolatedVowel(start, ipa, sound1)) {
            String rest = ipa.substring(start + sound1.length());
            out = ipa.substring(0, start) + sound2;
            if (rest.contains(sound1)) {
                start = rest.indexOf(sound1);
                if (isolatedVowel(start, rest, sound1)) {
                    String r = rest.substring(start + sound1.length());
                    out += rest.substring(0, start) + sound2 + r;
                } else {
                    out += rest;
                }
            } else {
                out += rest;
            }
        } else {
            out = ipa;
        }
        return out;
    }

    /**
     * check if sound is an isolated vowel group in ipa looking from start
     *
     * @param start
     * @param ipa
     * @param sound
     * @return
     */
    private boolean isolatedVowel(int start, String ipa, String sound) {
        return (start == 0
                || JsonbService.IPCONSONANTS.contains(ipa.substring(start - 1, start)))
                && (start + sound.length() == ipa.length()
                || JsonbService.IPCONSONANTS.contains(ipa.substring(start + sound.length(), start + sound.length() + 1)));
    }

    /**
     * Return max 4 similar sounding ipa's (half rhyme)
     *
     * @param ipa
     * @return
     */
    private List<String> findSimilarSound(String ipa) {
        final List<String> rv = new ArrayList<>(4);
        rv.add(ipa);
        similarIpa.forEach((key, value) -> {
            if (rv.size() < 4) {
                String firstTocompare = key.length() > value.length() ? key : value;
                String secondTocompare = key.length() > value.length() ? value : key;
                if (ipa.contains(firstTocompare)) {
                    String s = otherIpa(ipa, firstTocompare, secondTocompare);
                    if (!s.equals(ipa) && !s.isEmpty()) {
                        rv.add(s);
                    }
                } else if (ipa.contains(secondTocompare)) {
                    String s = otherIpa(ipa, secondTocompare, firstTocompare);
                    if (!s.equals(ipa) && !s.isEmpty()) {
                        rv.add(s);
                    }
                }
            }
        });
        return rv;
    }
    
}

class VowelGroup {

    final int start, end;
    final String ipa;
    
    public VowelGroup(int start, int end, String ipa) {
        this.start = start;
        this.end = end;
        this.ipa = ipa;
    }
    
}

class Similarity {

    private final int weight;
    private final String markedIpa;
    
    public Similarity(int weight, String markedIpa) {
        this.weight = weight;
        this.markedIpa = markedIpa;
    }
    
    public int getWeight() {
        return weight;
    }
    
    public String getMarkedIpa() {
        return markedIpa;
    }
    
}
