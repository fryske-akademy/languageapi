package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.RequestHelper;
import com.vectorprint.configuration.cdi.Property;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.NotFoundException;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.languagemodel.DetailsFetcher;
import org.fryske_akademy.languagemodel.EnglishHelper;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.JsonbToGqlMapper;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.Lemma;
import org.fryske_akademy.languagemodel.LemmasFetcher;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.SearchLogger;
import org.fryske_akademy.languagemodel.Source;
import org.fryske_akademy.languagemodel.SourcesFetcher;
import org.fryske_akademy.languagemodel.TextsFetcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class AbstractFetcher<T> implements DataFetcher<T> {

    private static final String MOLEXPATH = "https://sk.taalbanknederlands.inl.nl/LexiconService/lexicon/get_lemma_from_wordform?database=molex&case_sensitive=true&wordform=";
    private static final String MOLEXLEMMAREGEX = "<lemma>([^<]*)</lemma>";
    protected static final Pattern COMPLEXTERM = Pattern.compile("[\\s()~!]");
    protected static final Pattern WILDCARDS = Pattern.compile("[*?]");
    public static final String INFOTEXTPATH = "/info";
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFetcher.class);
    public static final String LANG = "lang";
    public static final String LEMMA = "lemma";
    public static final String SEARCHTERM = "searchterm";

    @Inject
    @Property(defaultValue = MOLEXPATH)
    private String molexUrl;
    @Inject
    @Property(defaultValue = MOLEXLEMMAREGEX)
    private Pattern molexRegex;
    @Inject
    @Property
    private String[] onfwServiceBaseUrl;
    @Inject
    @Property
    private String[] fhwbServiceBaseUrl;
    @Inject
    @Property
    private String[] fnwbServiceBaseUrl;
    @Inject
    @Property
    private String[] nfwbServiceBaseUrl;
    @Inject
    @Property
    private String[] jurwbServiceBaseUrl;
    @Inject
    @Property
    private String[] fiwbServiceBaseUrl;
    @Inject
    @Named("searchLogger")
    protected SearchLogger searchLogger;
    @Inject
    private SourcesFetcher sourcesFetcher;

    @Inject
    @Property(defaultValue = {"fhwb"})
    private String[] fryLemmaOrder;

    @Inject
    @Property(defaultValue = {"onfw", "nfwb"})
    private String[] nlLemmaOrder;

    @Inject
    @Property(defaultValue = {"onfw", "fhwb", "fiwb"})
    private String[] fryTextOrder;

    @Inject
    @Property(defaultValue = {"onfw"})
    private String[] nlTextOrder;

    @Inject
    @Property(defaultValue = "false")
    private boolean logLexica;

    @Inject
    @Property(defaultValue = "30")
    private int pagingMax;

    @Inject
    private FkwHelper fkwHelper;

    @Inject
    private EnglishHelper englishHelper;

    @Inject
    @Property(defaultValue = "300")
    private int timeoutOersetterSeconds;

    @Inject
    @Property(defaultValue = "30")
    private int timeoutSeconds;

    @Inject
    @Property(defaultValue = "5")
    private int timeoutSecondsMolex;

    @Inject
    @Property(defaultValue = "true")
    private boolean extendedLemmaSearch;

    public int getTimeoutOersetterSeconds() {
        return timeoutOersetterSeconds;
    }

    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    protected String stripSlash(String url) {
        return url.replaceAll("/+$", "");
    }

    protected HttpRequest.Builder headers(HttpRequest.Builder builder) {
        return builder.setHeader("User-Agent", "java.net.http.HttpClient")
                .header("Content-Type", "application/x-www-form-urlencoded");
    }

    /**
     * Subclasses can use this object for searching in dictionaries. It contains
     * either a list of dictionary url's for querying or a list of lemma's from
     * the lexicon to process.
     *
     * @see #getSearchInfo(DataFetchingEnvironment)
     */
    protected static class SearchInfo {

        private String origTerm;
        private final List<String> extendedTerms;
        private final List<String> urls;
        private final List<Lemma> fkwLemmas = new ArrayList<>(FkwHelper.MAXLEMMAS);

        public SearchInfo(List<String> urls, String... terms) {
            this.urls = Collections.unmodifiableList(urls);
            this.extendedTerms = Arrays.asList(terms);
        }

        public List<String> getUrls() {
            return urls;
        }

        public List<Lemma> getFkwLemmas() {
            return Collections.unmodifiableList(fkwLemmas);
        }

        private SearchInfo add(List<Lemma> fkwLemmas) {
            if (this.fkwLemmas.isEmpty()) {
                this.fkwLemmas.addAll(fkwLemmas);
            } else {
                throw new IllegalStateException("refused to add lemmas, lemmas present");
            }
            return this;
        }

        /**
         * return the sanitized searchterm possibly extended in the form "term1
         * OR term2...."
         *
         * @return
         */
        public String getQuery() {
            return String.join(" OR ", extendedTerms);
        }

        /**
         * return all sanitized terms that are used for searching
         *
         * @return
         */
        public List<String> getSearchTerms() {
            return extendedTerms;
        }

        public String getOrigTerm() {
            return origTerm;
        }

        private SearchInfo setOrigTerm(String origTerm) {
            this.origTerm = origTerm;
            return this;
        }
    }
    
    /**
     * For simple search terms (without whitespace or brackets) add
     * {@link FkwHelper#MAXLEMMAS max x} lemmas found in the frisian or dutch
     * lexicon. For frisian callers have two ways to trigger the use of lexicon
     * data:
     * <ul>
     * <li>an argument "lexiconFallback" with value true: means lexicon data
     * will be used as fallback when no match in dictionaries</li>
     * <li>an argument "source" with value "fkw": only lexicon will be
     * queried</li>
     * </ul>
     *
     * @param environment
     * @param term
     * @param baseUrls
     * @return
     */
    private SearchInfo extendTerm(DataFetchingEnvironment environment, String term, String[] baseUrls, boolean forDetails) {
        boolean simple = !COMPLEXTERM.matcher(term).find();
        if (simple) {
            String s = environment.getArgument("source");
            Source source = sourcesFetcher.getSource(s);
            String lang = environment.getArgument(LANG);
            LangType lt = lang!=null?LangType.valueOf(lang):source.getFromlang();
            boolean fry = LangType.fry == lt;
            boolean en = LangType.en==lt;
            boolean wildcards = WILDCARDS.matcher(term).find();
            Set<String> lemmas = new LinkedHashSet<>(islexicon(environment) ? 50 : FkwHelper.MAXLEMMAS);
            lemmas.add(term);
            List<Lemma> fkwLemmas = new ArrayList<>(islexicon(environment) ? 50 : FkwHelper.MAXLEMMAS);
            if (fry) {
                /*
                fetch lemmas from fkw, either for the purpose of extending the searchterm or when directly searching in fkw
                */
                List<Lemma> lemmata = (!SourcesFetcher.SOURCE.fkw.eqSource(source)&&wildcards) || forDetails ?
                        Collections.emptyList() :
                        lemmaFromFkw(environment, term, islexicon(environment) ? -1 : FkwHelper.MAXLEMMAS);
                if (forDetails&&SourcesFetcher.SOURCE.fkw.eqSource(source)) {
                    lemmata=lemmaFromFkw(environment, term, -1);
                }
                if (islexicon(environment) || environment.getArgumentOrDefault("lexiconFallback", Boolean.TRUE)) {
                    // for searching in fkw directly or when nothing found in dictionaries
                    fkwLemmas.addAll(lemmata);
                }
                if (!islexicon(environment)) {
                    // extend the searchterm with lemmas found in fkw
                    lemmas.addAll(lemmata.stream().map(Lemma::getForm).filter(
                            form -> wildcards || !term.equals(form)
                    ).toList());
                }
            } else if (en) {
                if (!wildcards && !forDetails) {
                    lemmas.addAll(lemmaForEnglish(term));
                }
            } else {
                if (!wildcards && !forDetails) {
                    lemmas.addAll(lemmaFromMolex(term));
                }
            }
            return forDetails ?
                    new SearchInfo(List.of(baseUrls), term).add(fkwLemmas) :
                    findLemmas(baseUrls, environment, true, fkwLemmas, lemmas.toArray(String[]::new));
        }
        return null;
    }

    private String termsQuery(String... terms) {
        String termQuery = Arrays.stream(terms)
                .limit(FkwHelper.MAXLEMMAS)
                .collect(Collectors.joining(" OR "));
        if (logLexica && terms.length > 1) {
            searchLogger.log("searching via lexicon for " + termQuery);
        }
        return termQuery;
    }

    /**
     * look for lemmas in several url's or in lexicon, may be called for details
     * or lemmas
     *
     * @param baseUrls
     * @param environment
     * @param recursing
     * @param fkwLemmas
     * @return
     * @throws NotFoundException
     */
    private SearchInfo findLemmas(String[] baseUrls, DataFetchingEnvironment environment, boolean recursing, List<Lemma> fkwLemmas, String... extendedTerms) throws NotFoundException {
        if (!recursing) {
            SearchInfo searchInfo = extendTerm(environment, extendedTerms[0], baseUrls, this instanceof DetailsFetcher);
            if (searchInfo != null) {
                return searchInfo;
            }
        }
        if (!(this instanceof DetailsFetcher)) {
            LangType lt = LangType.valueOf(environment.getArgument(LANG));
            if (islexicon(environment.getArgumentOrDefault("source", ""))) {
                checkLang(lt, environment.getArgumentOrDefault("source", ""));
            }
            return checkUrls(baseUrls,environment,termsQuery(extendedTerms)).add(fkwLemmas);
        }
        if (!fkwLemmas.isEmpty()) {
            if (logLexica) searchLogger.log(String.format("using lemmas from fkw for %s", extendedTerms[0]));
            return new SearchInfo(Collections.emptyList(), extendedTerms).add(fkwLemmas);
        }
        throw new NotFoundException(String.format("unable to find %s, %s in dictionaries",
                extendedTerms[0], environment.getArgument(LANG)));
    }

    private List<Lemma> lemmaFromFkw(DataFetchingEnvironment environment, String term, int max) {
        boolean notFkw = !SourcesFetcher.SOURCE.fkw.name().equals(environment.getArgument("source"));
        boolean notLemmaQuery = !(this instanceof LemmasFetcher);
        boolean synonymsRequested = notFkw && notLemmaQuery && synonymsRequested(environment);
        boolean variantsRequested = notFkw && notLemmaQuery && variantsRequested(environment);
        boolean dutchismsRequested = notFkw && notLemmaQuery && dutchismsRequested(environment);
        String pos = environment.getArgumentOrDefault("pos", "");
        boolean extended = notFkw && extendedLemmaSearch;
        return fkwHelper.findLemma(term, pos.isEmpty() ? null : Pos.valueOf(pos), max, extended).stream()
                .map(l -> jsonbToGqlMapper.map(l,
                        synonymsRequested,
                        variantsRequested,
                        dutchismsRequested))
                .toList();
    }

    private List<String> lemmaForEnglish(String term) {
        return englishHelper.lemmaFor(term);
    }

    private List<String> lemmaFromMolex(String term) {
        List<String> lemmas = new ArrayList<>(3);
        try {
            if (logLexica) {
                searchLogger.log("searching lemma in molex for " + term);
            }
            String resp = getRequestHelper().request(timeoutSecondsMolex,
                    HttpRequest.newBuilder().uri(URI.create(molexUrl + term)).GET().build()
            ).get(0);
            if (resp != null) {
                Matcher matcher = molexRegex.matcher(resp);
                while (matcher.find()) {
                    String molexLemma = matcher.group(1);
                    if (!term.equals(molexLemma)) {
                        lemmas.add(molexLemma);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.warn(String.format("unable to find %s in molex",
                    term), e);
        }
        return lemmas;
    }

    private SearchInfo checkUrls(String[] baseUrls, DataFetchingEnvironment environment, String term) throws NotFoundException {
        LangType lt = LangType.valueOf(environment.getArgument(LANG));
        final List<String> list = Arrays.stream(baseUrls).map(this::stripSlash).peek(u -> checkLang(lt, getSourceId(u))).toList();
        return new SearchInfo(list, term);
    }

    private void checkLang(LangType lang, String sourceId) {
        Source source = sourcesFetcher.getSource(sourceId);
        if (source != null && !source.getFromlang().equals(lang) && !source.getTolang().equals(lang)) {
            throw new NotFoundException(sourceId + " does not support " + lang);
        }
    }

    protected String getSourceId(String url) {
        if (url.startsWith(onfwServiceBaseUrl[0])) {
            return onfwServiceBaseUrl[1];
        } else if (url.startsWith(fhwbServiceBaseUrl[0])) {
            return fhwbServiceBaseUrl[1];
        } else if (url.startsWith(fnwbServiceBaseUrl[0])) {
            return fnwbServiceBaseUrl[1];
        } else if (url.startsWith(nfwbServiceBaseUrl[0])) {
            return nfwbServiceBaseUrl[1];
        } else if (url.startsWith(jurwbServiceBaseUrl[0])) {
            return jurwbServiceBaseUrl[1];
        } else if (url.startsWith(fiwbServiceBaseUrl[0])) {
            return fiwbServiceBaseUrl[1];
        } else {
            throw new IllegalArgumentException("no know dictionary url " + url);
        }
    }

    protected String getSourceUrl(String id) {
        if (id.equals(onfwServiceBaseUrl[1])) {
            return onfwServiceBaseUrl[0];
        } else if (id.equals(fhwbServiceBaseUrl[1])) {
            return fhwbServiceBaseUrl[0];
        } else if (id.equals(fnwbServiceBaseUrl[1])) {
            return fnwbServiceBaseUrl[0];
        } else if (id.equals(nfwbServiceBaseUrl[1])) {
            return nfwbServiceBaseUrl[0];
        } else if (id.equals(jurwbServiceBaseUrl[1])) {
            return jurwbServiceBaseUrl[0];
        } else if (id.equals(fiwbServiceBaseUrl[1])) {
            return fiwbServiceBaseUrl[0];
        } else {
            throw new IllegalArgumentException("no know dictionary id " + id);
        }
    }

    private String[] getUrls(String... order) {
        return Arrays.stream(order)
                .map(this::getSourceUrl)
                .toList().toArray(new String[order.length]);
    }

    public final static Pattern DIACRITICS = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

    public final static Pattern WS = Pattern.compile("\\s+");

    public final static Pattern SPACEQUESTIONSTAR = Pattern.compile(" ([*?])");

    public static final Pattern AND = Pattern.compile(OPERATOR.AND.getUserInput());

    /**
     * remove diacrits, remove " OR ", keep " AND ", lowerCase, trim, collapse
     * whitespace, remove space from " [?*]"
     *
     * @param t
     * @return
     */
    public static String sanitizeTerm(String t) {
        final StringBuilder term = new StringBuilder();
        String clean = t.contains(OPERATOR.OR.getUserInput())
                ? t.trim().replaceAll(OPERATOR.OR.getUserInput(), " ")
                : t.trim();
        Matcher matcher = WS.matcher(clean);
        if (matcher.find()) {
            clean = matcher.replaceAll(" ");
        }
        matcher = SPACEQUESTIONSTAR.matcher(clean);
        if (matcher.find()) {
            clean = matcher.group(1);
        }

        final AtomicInteger index = new AtomicInteger();
        new Scanner(clean).useDelimiter(AND).forEachRemaining(s -> {
            if (index.getAndIncrement() > 0) {
                term.append(OPERATOR.AND.getUserInput());
            }
            term.append(removeDiacrits(s));
        });
        return term.toString();
    }

    private static final Pattern TOQUOTE = Pattern.compile("[()/]");

    private String quote(String term) {
        return term.startsWith("\"") && term.endsWith("\"") ? term : "\"" + term + "\"";
    }

    public static String removeDiacrits(String t) {
        return DIACRITICS.matcher(Normalizer.normalize(t, Normalizer.Form.NFKD))
                .replaceAll("").toLowerCase(Locale.ROOT);
    }

    @Inject
    @Property(defaultValue = "3")
    private int minCharsForWildcards;

    @Inject
    @Property(defaultValue = "75")
    private int maxChars;

    private static final Pattern CHARS = Pattern.compile("[a-zA-Z]");

    @Inject
    private JsonbToGqlMapper jsonbToGqlMapper;

    protected boolean islexicon(DataFetchingEnvironment environment) {
        return islexicon(environment.getArgumentOrDefault("source", ""));
    }

    private boolean islexicon(String source) {
        Source fetcherSource = sourcesFetcher.getSource(source);
        return fetcherSource != null && fetcherSource.getLexicon();
    }

    protected SearchInfo getSearchInfo(DataFetchingEnvironment environment, String t) throws NotFoundException {
        return _getSearchInfo(environment, t).setOrigTerm(t);
    }

    private SearchInfo _getSearchInfo(DataFetchingEnvironment environment, String t) throws NotFoundException {
        String term = this instanceof DetailsFetcher ? t : sanitizeTerm(t);
        if (term.length() > maxChars) {
            throw new IllegalArgumentException(String.format("The search is longer than %d characters", maxChars));
        }
        if (term.contains("*")) {
            Matcher matcher = CHARS.matcher(term);
            short i = 0;
            while (matcher.find() && i < minCharsForWildcards) {
                i++;
            }
            if (i < minCharsForWildcards) {
                throw new IllegalArgumentException(String.format("Using \"*\" requires a minimum of %d letters", minCharsForWildcards));
            }
        }

        if ("en".equals(environment.getArgument(LANG))&&TOQUOTE.matcher(term).find()) {
            // TODO data in frisian english dictionary should be edited
            term = quote(term);
        }

        String source = environment.getArgumentOrDefault("source", "");
        if (source.equals("null")) {
            source = "";
        }
        if (!source.isEmpty() && !islexicon(source)) {
            // TODO this call is confusing when fetching details or texts
            SearchInfo extendTerm = this instanceof TextsFetcher ? null :
                    extendTerm(environment, term, new String[]{getSourceUrl(source)}, this instanceof DetailsFetcher);
            return extendTerm == null ?
                    new SearchInfo(List.of(getSourceUrl(source)), term) :
                    extendTerm;
        }
        if (islexicon(source) && (this instanceof DetailsFetcher)) {
            return findLemmas(EMPTY, environment, false, Collections.emptyList(), term);
        }
        if (TextsFetcher.TEXTSEARCH.equals(environment.getFieldDefinition().getName())) {
            if (islexicon(source)) {
                throw new NotFoundException(String.format("%s contains no texts", environment.getArgumentOrDefault("source", "")));
            }
            if ("nl".equals(environment.getArgument(LANG))) {
                return checkUrls(islexicon(source) ? EMPTY : getUrls(nlTextOrder), environment, term);
            } else if ("en".equals(environment.getArgument(LANG))) {
                return checkUrls(islexicon(source) ? EMPTY : getUrls("fiwb"), environment, term);
            } else {
                return checkUrls(islexicon(source) ? EMPTY : getUrls(fryTextOrder), environment, term);
            }
        }
        if ("nl".equals(environment.getArgument(LANG))) {
            return findLemmas(getUrls(nlLemmaOrder), environment, false, Collections.emptyList(), term);
        } else if ("en".equals(environment.getArgument(LANG))) {
            return findLemmas(getUrls("fiwb"), environment, false, Collections.emptyList(), term);
        } else {
            return findLemmas(islexicon(source) ? EMPTY : getUrls(fryLemmaOrder), environment, false, Collections.emptyList(), term);
        }
    }

    private static final String[] EMPTY = new String[]{};

    protected SearchInfo getSearchInfo(DataFetchingEnvironment environment) throws NotFoundException {
        return getSearchInfo(environment, environment.getArgument(LEMMA));
    }

    protected RequestHelper getRequestHelper() {
        return new RequestHelper() {
            @Override
            public List<String> request(int timeoutSeconds, HttpRequest... requests) throws ExecutionException, InterruptedException {
                try (this) {
                    return super.request(timeoutSeconds, requests);
                } catch (TimeoutException e) {
                    LOGGER.warn(String.format("timeout for request to One of %s",
                            Arrays.stream(requests).map(r -> r.uri().toString()).collect(Collectors.joining(", "))
                    ));
                }
                return Collections.emptyList();
            }
        };
    }

    protected String sensitiveParam(DataFetchingEnvironment environment) {
        return sensitiveParam(environment.getArgumentOrDefault("sensitive", Boolean.FALSE));
    }

    protected String sensitiveParam(boolean sensitive) {
        return sensitive ? "&sensitive=true" : "";
    }

    protected int getPagingMax() {
        return pagingMax;
    }

    static boolean synonymsRequested(DataFetchingEnvironment environment) {
        return envContains(environment, "**Synonym**form");
    }

    static protected boolean variantsRequested(DataFetchingEnvironment environment) {
        return envContains(environment, "**Variant**form");
    }

    static protected boolean dutchismsRequested(DataFetchingEnvironment environment) {
        return envContains(environment, "**Dutchism**form");
    }

    private static boolean envContains(DataFetchingEnvironment environment, String path) {
        return environment.getSelectionSet().contains(path);
    }

}
