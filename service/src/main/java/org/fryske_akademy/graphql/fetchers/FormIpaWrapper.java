package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 - 2023 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.fa.foarkarswurdlist.ejb.FormIpa;

import java.util.List;
import java.util.Objects;


public class FormIpaWrapper extends FormIpa {

    private final Similarity similarity;
    private final String halfRhymeEnding;
    public FormIpaWrapper(FormIpa formIpa, Similarity similarity, boolean halfRhyme, List<String> vowelGroups) {
        super(formIpa.getForm(), formIpa.getIpa(), formIpa.getHyphenation(), formIpa.getLemma(), formIpa.isVariant(), formIpa.getPos());
        this.similarity = similarity;
        this.halfRhymeEnding = halfRhyme ? vowelGroups.get(vowelGroups.size()-1) : "AAAA";
    }

    public Similarity getSimilarity() {
        return similarity;
    }

    public String getHalfRhymeEnding() {
        return halfRhymeEnding;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.getForm());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormIpa other = (FormIpa) obj;
        return this.getForm().equals(other.getForm());
    }
    
    
}