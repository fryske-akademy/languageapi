package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.schema.DataFetchingEnvironment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.xml.bind.JAXBElement;
import org.fryske_akademy.languagemodel.Collocation;
import org.fryske_akademy.languagemodel.DefContent;
import org.fryske_akademy.languagemodel.Definition;
import org.fryske_akademy.languagemodel.Details;
import org.fryske_akademy.languagemodel.Dutchism;
import org.fryske_akademy.languagemodel.Example;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.FormHelper;
import org.fryske_akademy.languagemodel.FormInfo;
import org.fryske_akademy.languagemodel.FormattedText;
import org.fryske_akademy.languagemodel.I;
import org.fryske_akademy.languagemodel.L;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.Lemma;
import org.fryske_akademy.languagemodel.LemmaLink;
import org.fryske_akademy.languagemodel.LemmaOrText;
import org.fryske_akademy.languagemodel.LemmasFetcher;
import org.fryske_akademy.languagemodel.NameType;
import org.fryske_akademy.languagemodel.Note;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.Proverb;
import org.fryske_akademy.languagemodel.SourcesFetcher;
import org.fryske_akademy.languagemodel.SubForm;
import org.fryske_akademy.languagemodel.Synonym;
import org.fryske_akademy.languagemodel.T;
import org.fryske_akademy.languagemodel.TeiToDetailsMapper;
import org.fryske_akademy.languagemodel.Text;
import org.fryske_akademy.languagemodel.TextTranslated;
import org.fryske_akademy.languagemodel.TextTypeI;
import org.fryske_akademy.languagemodel.TextTypeQ;
import org.fryske_akademy.languagemodel.Usg;
import org.fryske_akademy.languagemodel.UsgType;
import org.fryske_akademy.languagemodel.Variant;
import org.fryske_akademy.teidictionaries.jaxb.Body;
import org.fryske_akademy.teidictionaries.jaxb.Cit;
import org.fryske_akademy.teidictionaries.jaxb.Def;
import org.fryske_akademy.teidictionaries.jaxb.Entry;
import org.fryske_akademy.teidictionaries.jaxb.Etym;
import org.fryske_akademy.teidictionaries.jaxb.Form;
import org.fryske_akademy.teidictionaries.jaxb.Gloss;
import org.fryske_akademy.teidictionaries.jaxb.Gram;
import org.fryske_akademy.teidictionaries.jaxb.Hi;
import org.fryske_akademy.teidictionaries.jaxb.ModelPtrLike;
import org.fryske_akademy.teidictionaries.jaxb.ObjectFactory;
import org.fryske_akademy.teidictionaries.jaxb.Q;
import org.fryske_akademy.teidictionaries.jaxb.Quote;
import org.fryske_akademy.teidictionaries.jaxb.Sense;
import org.fryske_akademy.teidictionaries.jaxb.TEI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@ApplicationScoped
public class TeiToDetailsMapperImpl implements TeiToDetailsMapper {

    /*

- foar it sykje nei lemma en it toanen fan details:
  - by taal nl jou de resultaten it earste fûn yn onfw, nfwb
  - by taal nl jou de resultaten it earste fûn yn (onfw, )fhwb, fnwb
- foar it sykje yn tekst allinnich sykje yn onfw

ophalen uit:

+- paradigm: onfw, fkw
- stress: fhwb
+- synonyms: fkw
+- variants: fkw
- pronunciation: grapheme2phoneme
+- hyphenation: fkw
+- article: fkw

- (expensive) pron and stress behind button
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(TeiToDetailsMapperImpl.class);

    @Inject
    private FkwHelper fkwHelper;

    @Inject
    private LemmasFetcher lemmasFetcher;

    @Override
    public List<Details> fromTei(TEI tei, Pos gramType, String sourceId, DataFetchingEnvironment environment) {
        if (tei != null) {
            Body body = tei.getText().getBody();
            if (body.getSuperEntry() != null) {
                // process entries with same pos
                return body.getSuperEntry().getEntries().stream()
                        .filter(entry -> samePos(entry, gramType))
                        .map(entry -> entryToDetails(entry, sourceId, environment))
                        .toList();
            } else {
                if (samePos(body.getEntry(), gramType)) {
                    return List.of(entryToDetails(body.getEntry(), sourceId, environment));
                }
            }
        }
        return null;
    }

    private static final JAXBElement<ModelPtrLike> POINTER = new ObjectFactory().createRef(null);

    private Details entryToDetails(Entry entry, String sourceId, DataFetchingEnvironment environment) {
        List<Cit> entryCits = fromContent(entry.getNotesAndSensesAndCits(), Cit.class);
        List<Sense> senses = new ArrayList<>();
        Map<Integer, org.fryske_akademy.teidictionaries.jaxb.Note> groupNotes = new HashMap<>();
        entry.getNotesAndSensesAndCits().forEach(o -> {
            if (o instanceof Cit senseGroup && senseGroup.getType() == Cit.CitType.sensegroup) {
                int i = senses.size();
                senses.addAll((senseGroup).getSenses());
                if (senseGroup.getNote() != null) {
                    groupNotes.put(i, senseGroup.getNote());
                }
            } else if (o instanceof Sense s) {
                senses.add(s);
            }
        });
        return Details.builder()
                // lemma
                .setLemma(lemmaGraph(getPos(entry), entry.getForm(),
                        fromContent(entry.getNotesAndSensesAndCits(), org.fryske_akademy.teidictionaries.jaxb.Note.class),
                        sourceId, environment))
                // texts
                .setTexts(textsFromEntry(entry, entryCits, sourceId))
                // link
                .setLink(linkFromEntry(entry, sourceId))
                // translations
                .setTranslations(getFormTranslations(getPos(entry), entryCits, entry.getForm(), sourceId, environment))
                // senses
                .setSenses(senses(groupNotes, senses, entry, entry.getForm().getLang(), sourceId, environment)
                ).setSource(sourceId).build();
    }

    private LemmaLink linkFromEntry(Entry entry, String sourceId) {
        return getLink((JAXBElement<ModelPtrLike>) entry.getNotesAndSensesAndCits().stream()
                .filter(o -> isElement(o, POINTER)).findFirst().orElse(null),
                getPos(entry), LangType.valueOf(entry.getForm().getLang()), sourceId);
    }

    /**
     * Pattern for suggested lemma link format (morgen.xml#pid_296371), first
     * group is the lemma, second is the id
     */
    public static final Pattern TARGET = Pattern.compile("(.*)\\.xml#(.*)");

    /**
     * Pattern for suggested lemma link format (morgen.xml), first group is the
     * lemma
     */
    public static final Pattern TARGET2 = Pattern.compile("(.*)\\.xml");

    @Override
    public LemmaId fromString(String lemmaRef) {
        Matcher matcher = TARGET.matcher(lemmaRef);
        Matcher matcher2 = TARGET2.matcher(lemmaRef);
        return matcher.find() ? new LemmaId(matcher.group(1), matcher.group(2))
                : matcher2.find() ? new LemmaId(matcher2.group(1), null) : new LemmaId(lemmaRef, lemmaRef);
    }

    private LemmaLink getLink(JAXBElement<ModelPtrLike> ptr, Pos pos, LangType langType, String sourceId) {
        if (ptr == null) {
            return null;
        }
        String ref = ptr.getValue().getTargets().stream().findFirst().orElse(getText(Collections.singletonList(ptr)));
        LemmaId lid = fromString(ref);
        String text = COPY_OF.matcher(getText(ptr.getValue().getContent()))
                .replaceAll("");
        return LemmaLink.builder()
                .setSource(sourceId)
                .setText(text.equals(lid.getLemma()) ? null : text)
                .setLang(langType)
                .setPos(pos)
                .setLemma(lid.getLemma())
                .setId(lid.getId())
                .build();
    }

    private boolean isElement(Object o, JAXBElement jb) {
        return o instanceof JAXBElement && ((JAXBElement) o).getName().equals(jb.getName());
    }

    private static final Pattern BEFORE_FORM = Pattern.compile("^\\(.*\\) ");

    private String stripBefore(String form, String sourceId) {
        if (!SourcesFetcher.SOURCE.nfwb.name().equals(sourceId)) {
            return form;
        }
        Matcher m = BEFORE_FORM.matcher(form);
        return m.find() ? m.replaceAll("") : form;
    }

    private Lemma lemmaGraph(Pos pos, Form form, List<org.fryske_akademy.teidictionaries.jaxb.Note> notes, String sourceId, DataFetchingEnvironment environment) {
        Lemma lemma = new Lemma();
        LangType lt = LangType.valueOf(form.getLang());
        boolean fry = lt == LangType.fry;
        // lang
        lemma.setLang(lt);
        // replace start capital
        String o = stripBefore(getOrth(form), sourceId);
        lemma.setForm(replaceCapital(fkwHelper, fry, pos, o, SourcesFetcher.SOURCE.nfwb.name().equals(sourceId)));
        // hyph
        if (fry) {
            lemma.setHyphenation(fkwHelper.getHyphenation(lemma.getForm()));
            lemma.setPronunciation(fkwHelper.getPronunciation(lemma.getForm()));
        }
        lemma.setPos(pos);
        // usg
        lemma.setUsage(usageFromForm(form));
        // namekind
        lemma.setNamekind(form.getNamekind() == null ? null : NameType.valueOf(form.getNamekind().name()));
        // article
        if (pos == Pos.noun || pos == Pos.x) {
            lemma.setArticle(fry ? fkwHelper.getArticle(lemma.getForm()) : getArticle(form));
        }
        if (fry) {
            List<Class<? extends SubForm>> sf = new ArrayList<>(4);
            if (AbstractFetcher.synonymsRequested(environment)) {
                sf.add(Synonym.class);
            }
            if (AbstractFetcher.variantsRequested(environment)) {
                sf.add(Variant.class);
            }
            if (AbstractFetcher.dutchismsRequested(environment)) {
                sf.add(Dutchism.class);
            }
            List<SubForm> subForms = getSubForms(fkwHelper, lemma, pos, sf);
            List<Variant> variants = addVariants(form, subForms.stream()
                    .filter(Variant.class::isInstance)
                    .map(s -> (Variant) s)
                    .collect(Collectors.toCollection(ArrayList::new)));
            subForms = subForms.stream()
                    .filter(s -> !(s instanceof Variant)).collect(Collectors.toCollection(ArrayList::new));
            subForms.addAll(variants);
            lemma.setSubForms(subForms.isEmpty() ? null : subForms);
        }
        // notes
        List<org.fryske_akademy.teidictionaries.jaxb.Note> noteList = fromContent(form.getPronsAndStressesAndForms(), org.fryske_akademy.teidictionaries.jaxb.Note.class);
        if (!noteList.isEmpty()) {
            notes.addAll(noteList);
        }
        lemma.setNote(getNote(notes));
        return lemma;
    }

    @Override
    public List<SubForm> getSubForms(FkwHelper fkwHelper, Lemma lemma, Pos pos, List<Class<? extends SubForm>> forms) {
        String form =  lemma.getForm();
        if (Pos.x==pos) pos = null;
        // paradigm/synonyms/variants
        final List<FormInfo> paradigm = fkwHelper.getParadigm(form, pos);
        List<SubForm> subForms = new ArrayList<>(paradigm);
        if (pos==null) {
            lemma.setPos(FormHelper.detectPos(paradigm.stream().map(FormInfo::getLinguistics).toArray(org.fryske_akademy.languagemodel.Form[]::new )));
        }
        if (forms == null || forms.contains(Synonym.class)) {
            subForms.addAll(fkwHelper.getSynonyms(form, pos));
        }
        if (forms == null || forms.contains(Variant.class)) {
            subForms.addAll(fkwHelper.getVariants(form, pos));
        }
        if (forms == null || forms.contains(Dutchism.class)) {
            subForms.addAll(fkwHelper.getDutchisms(form, pos));
        }
        return subForms;
    }

    private String getArticle(Form form) {
        return form == null ? null : form.getArticle();
    }

    static String replaceCapital(FkwHelper fkwHelper, boolean fry, Pos pos, String o, boolean nfwb) {
        if (nfwb && fry && CAPITAL.matcher(o).find() && Pos.propn != pos) {
            List<org.fa.foarkarswurdlist.jpa.jsonb.Lemma> lm = fkwHelper.findLemma(o, pos, FkwHelper.MAXLEMMAS, false);
            return lm.isEmpty() ? o : lm.get(0).getForm();
        }
        return o;
    }

    private Pos getPos(Entry entry) {
        Pos pos_ = grammar(entry.getForm());
        if (pos_ != null) {
            return pos_;
        } else {
            Optional<Cit> cit = fromContent(entry.getNotesAndSensesAndCits(), Cit.class).stream()
                    .filter(c -> c.getType() == Cit.CitType.translation).findFirst();
            if (cit.isPresent()) {
                return posFromCit(cit.get());
            } else {
                cit = fromContent(fromContent(entry.getNotesAndSensesAndCits(), Sense.class).stream().findFirst()
                        .orElseThrow(
                                () -> new IllegalStateException(
                                        "Missing pos for " + getOrth(entry.getForm())
                                )
                        ).getRevesAndDevesAndCits(), Cit.class).stream().findFirst();
                if (cit.isPresent()) {
                    return posFromCit(cit.get());
                } else {
                    throw new IllegalStateException(
                            "Missing pos for " + getOrth(entry.getForm())
                    );
                }
            }
        }
    }

    private Pos posFromCit(Cit cit) {
        if (cit.getForms().isEmpty()) {
            throw new IllegalStateException("missing form in translation");
        }
        return grammar(cit.getForms().get(0));
    }

    private List<Variant> addVariants(Form form, List<Variant> variants) {
        fromContent(form.getPronsAndStressesAndForms(), Form.class).forEach(o -> {
            if (o.getType() == Form.FormType.variant) {
                if (variants.stream().noneMatch(v -> v.getForm().equals(getOrth(o)))) {
                    variants.add(Variant.builder()
                            .setLang(LangType.valueOf(form.getLang()))
                            .setForm(getOrth(o))
                            .build());
                }
            }
        });
        return variants;
    }

    private List<Usg> usageFromForm(Form form) {
        return getUsg(firstFromContent(form.getPronsAndStressesAndForms(), Etym.class).orElse(null));
    }

    private <T> List<T> fromContent(List content, Class<T> clazz) {
        List<T> l = new ArrayList<>();
        content.stream().filter(o -> clazz.isAssignableFrom(o.getClass())).forEach(o -> l.add((T) o));
        return l;
    }

    private <T> Optional<T> firstFromContent(List content, Class<T> clazz) {
        return fromContent(content, clazz).stream().findFirst();
    }

    private List<org.fryske_akademy.languagemodel.Sense> senses(Map<Integer, org.fryske_akademy.teidictionaries.jaxb.Note> groupNotes, List<Sense> senseList, Entry entry, String lang, String sourceId, DataFetchingEnvironment environment) {
        List<org.fryske_akademy.languagemodel.Sense> senses = new ArrayList<>();
        final List<Lemma> lastTranslations = new ArrayList<>(3);
        for (int i = 0; i < senseList.size(); i++) {
            Sense sense = senseList.get(i);
            List<LemmaOrText> l = new ArrayList<>(3);
            String article = getArticle(sense.getForm());
            if (entry == null) {
                fromContent(sense.getRevesAndDevesAndCits(), Cit.class)
                        .stream().filter(c -> c.getType() == Cit.CitType.translation).findFirst().ifPresent(cit -> l.addAll(citTranslation( null, cit)));
            } else {
                if (article != null && article.equals(getArticle(entry.getForm()))) {
                    article = null;
                }
                List<Lemma> tl = getFormTranslations(getPos(entry),fromContent(sense.getRevesAndDevesAndCits(),
                        Cit.class),entry.getForm(),sourceId,environment);
                l.addAll(tl.isEmpty() ? lastTranslations : tl);
                if (!tl.isEmpty()) {
                    lastTranslations.clear();
                    lastTranslations.addAll(tl);
                }
            }
            List<Text> texts = textsFromSense(sense, lang, sourceId);
            Definition def = def(fromContent(sense.getRevesAndDevesAndCits(), Def.class));
//   TODO test this         if (def==null && texts!=null && !texts.isEmpty() && texts.get(0) instanceof Example) {
//                // take first example as definition
//                org.fryske_akademy.languagemodel.Gloss g =
//                        org.fryske_akademy.languagemodel.Gloss.builder()
//                                .setGloss(((Example)texts.get(0)).getText()).build();
//                def = Definition.builder()
//                        .setDef(List.of(g))
//                        .build();
//            }
            senses.add(org.fryske_akademy.languagemodel.Sense.builder()
                    .setId(sense.getId())
                    .setArticle(article)
                    .setDefinition(def)
                    .setTexts(texts)
                    .setGroupNote(groupNotes != null && groupNotes.containsKey(i)
                            ? getNote(groupNotes.get(i))
                            : null)
                    .setNotes(notesFromSense(sense))
                    .setTranslations(l.isEmpty() ? null : l)
                    .setLink(sense.getRevesAndDevesAndCits().stream()
                            .filter(o -> isElement(o, POINTER))
                            .map(ref -> getLink((JAXBElement<ModelPtrLike>) ref, null,
                            LangType.valueOf(lang), sourceId)).toList())
                    .build());
        }
        return senses;
    }

    private Definition def(List<Def> defs) {
        if (defs == null || defs.isEmpty()) {
            return null;
        }
        List<DefContent> content = new ArrayList<>();
        defs.forEach(o -> textFromDef(content, o));
        return Definition.builder()
                .setDef(content)
                .setUsage(usgFromDef(defs.isEmpty() ? null : defs.get(0)))
                .build();
    }

    private List<Note> notesFromSense(Sense sense) {
        return fromContent(sense.getRevesAndDevesAndCits(), org.fryske_akademy.teidictionaries.jaxb.Note.class)
                .stream().map(n -> {
                    FormattedText FormattedText = new FormattedText(new ArrayList<>());
                    textFromNote(FormattedText, n);
                    return Note.builder().setText(FormattedText).build();
                }).toList();
    }

    private Note getNote(org.fryske_akademy.teidictionaries.jaxb.Note note) {
        return note == null ? null : getNote(List.of(note));
    }

    private static final Pattern COPY_OF = Pattern.compile("^copy of\\s*\\r?\\s*");

    private Note getNote(List<org.fryske_akademy.teidictionaries.jaxb.Note> notes) {
        if (!notes.isEmpty() && notes.get(0) != null) {
            Note note = new Note();
            note.setText(new FormattedText(new ArrayList<>()));
            notes.stream().filter(Objects::nonNull).forEach(o -> textFromNote(note.getText(), o));
            return note;
        }
        return null;
    }

    private List<Text> textsFromEntry(Entry entry, List<Cit> cits, String sourceId) {
        return getTexts(LangType.valueOf(entry.getForm().getLang()), cits, sourceId);
    }

    private List<Text> textsFromSense(Sense sense, String lang, String sourceId) {
        List<Cit> cits = fromContent(sense.getRevesAndDevesAndCits(), Cit.class);
        return getTexts(LangType.valueOf(lang), cits, sourceId);
    }

    List<org.fryske_akademy.teidictionaries.jaxb.Note> toList(org.fryske_akademy.teidictionaries.jaxb.Note note) {
        return note==null?Collections.emptyList():List.of(note);
    }

    private List<Text> getTexts(LangType lt, List<Cit> cits, String sourceId) {
        List<Text> texts = new ArrayList<>();
        if (!cits.isEmpty()) {
            cits.forEach(c -> {
                List<org.fryske_akademy.teidictionaries.jaxb.Note> notes = toList(c.getNote());
                // model forces one quote for type other than translation
                c.getQuotes().stream().findFirst().ifPresent(q -> {
                    List<org.fryske_akademy.teidictionaries.jaxb.Note> merged = new ArrayList<>(notes);
                    merged.addAll(fromContent(q.getContent(), org.fryske_akademy.teidictionaries.jaxb.Note.class));
                    switch (c.getType()) {
                        case example:
                            texts.add(Example.builder()
                                    .setId(c.getId())
                                    .setLang(lt)
                                    .setNote(getNote(merged))
                                    .setTranslations(textTranslation(c))
                                    .setText(textFromQuote(q)).build());
                            break;
                        case proverb:
                            texts.add(Proverb.builder()
                                    .setId(c.getId())
                                    .setDefinition(def(c.getDeves()))
                                    .setLang(lt)
                                    .setNote(getNote(merged))
                                    .setTranslations(textTranslation(c))
                                    .setText(textFromQuote(q)).build());
                            break;
                        case collocation:
                            List<Cit> sensegroup
                                    = c.getCits().stream().filter(e -> e.getType() == Cit.CitType.sensegroup)
                                            .limit(1)
                                            .toList();
                            List<org.fryske_akademy.languagemodel.Sense> senses = new ArrayList<>(2);
                            if (!sensegroup.isEmpty()) {
                                senses.addAll(senses(null, sensegroup.get(0).getSenses(), null, lt.name(), sourceId, null));
                            }
                            List<Example> examples = new ArrayList<>();
                            c.getCits().stream().filter(e -> e.getType() == Cit.CitType.example).forEach(ce -> {
                                List<org.fryske_akademy.teidictionaries.jaxb.Note> ns = toList(ce.getNote());
                                ce.getQuotes().forEach(qe -> {
                                    List<org.fryske_akademy.teidictionaries.jaxb.Note> me = new ArrayList<>(ns);
                                    me.addAll(fromContent(qe.getContent(), org.fryske_akademy.teidictionaries.jaxb.Note.class));
                                    examples.add(Example.builder()
                                            .setId(ce.getId())
                                            .setLang(lt)
                                            .setNote(getNote(me))
                                            .setTranslations(textTranslation(ce))
                                            .setText(textFromQuote(qe)).build());
                                });

                            });
                            if (!sensegroup.isEmpty() && !examples.isEmpty()) {
                                LOGGER.warn(String.format("unexpected examples found in collocation with multiple senses for \"%s\"", textFromQuote(q).getText().stream().
                                        filter(T.class::isInstance)
                                        .map(t -> ((T) t).getTextT()).collect(Collectors.joining(" "))));
                            }
                            texts.add(Collocation.builder()
                                    .setId(c.getId())
                                    .setLang(lt)
                                    .setDefinition(def(c.getDeves()))
                                    .setNote(getNote(merged))
                                    .setSenses(senses.isEmpty() ? null : senses)
                                    .setUsage(usgFromQuote(q))
                                    .setTranslations(textTranslation(c))
                                    .setExamples(examples.isEmpty() ? null : examples)
                                    .setText(textFromQuote(q)).build());
                            break;
                    }
                });
            });
            return texts;
        }
        return null;
    }

    private List<TextTranslated> textTranslation(Cit cit) {
        List<TextTranslated> texts = new ArrayList<>();
        cit.getCits().stream().filter(c -> c.getType() == Cit.CitType.translation).forEach(c -> citTranslation(texts, c));
        return texts;
    }

    private List<LemmaOrText> citTranslation(List<TextTranslated> texts, Cit c) {
        List<TextTranslated> rv = texts == null ? new ArrayList<TextTranslated>(3) : texts;
        List<org.fryske_akademy.teidictionaries.jaxb.Note> notes = toList(c.getNote());
        LangType langType = LangType.valueOf(c.getLang());
        c.getQuotes().forEach(q -> {
            List<org.fryske_akademy.teidictionaries.jaxb.Note> merged = new ArrayList<>(notes);
            merged.addAll(fromContent(q.getContent(), org.fryske_akademy.teidictionaries.jaxb.Note.class));
            rv.add(TextTranslated.builder()
                    .setId(c.getId())
                    .setLang(langType)
                    .setNote(getNote(merged))
                    .setUsage(usgFromQuote(q))
                    .setText(textFromQuote(q)).build());
        });
        return new ArrayList<LemmaOrText>(rv);
    }

    private List<Usg> usgFromQuote(Quote q) {
        return getUsg(firstFromContent(q.getContent(), Etym.class).orElse(null));
    }

    private List<Usg> usgFromDef(Def d) {
        return d == null ? null : getUsg(firstFromContent(d.getContent(), Etym.class).orElse(null));
    }

    private List<Usg> getUsg(Etym etym) {
        if (etym != null) {
            return fromContent(etym.getContent(), org.fryske_akademy.teidictionaries.jaxb.Usg.class).stream()
                    .map(u -> Usg.builder()
                    .setType(UsgType.valueOf(u.getType()))
                    .setText(getText(u.getContent()))
                    .build()).toList();
        }
        return null;
    }

    private static final Pattern CAPITAL = Pattern.compile("^[A-Z]");

    private List<Lemma> getFormTranslations(Pos pos, List cits, Form form, String sourceId, DataFetchingEnvironment environment) {
        List<Lemma> lemmas = new ArrayList<>();
        final boolean english = environment.getArgumentOrDefault("englishTranslations", Boolean.FALSE);
        final boolean fiwb = SourcesFetcher.SOURCE.fiwb.name().equals(sourceId);
        cits.forEach(o -> {
            Cit c = (Cit) o;
            org.fryske_akademy.teidictionaries.jaxb.Note n = c.getNote();
            if (c.getType() == Cit.CitType.translation) {
                c.getForms().forEach(f -> {
                    LangType lt = LangType.valueOf(f.getLang());
                    String l = getOrth(f);
                    if (lt == LangType.fry && LemmasFetcherImpl.LATIN.matcher(l).find()) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Skipping latin: {}", l);
                        }
                        return;
                    }
                    List<org.fryske_akademy.teidictionaries.jaxb.Note> notes = new ArrayList<>(1);
                    if (n != null) {
                        notes.add(n);
                    }
                    lemmas.add(lemmaGraph(pos, f, notes, sourceId, environment));
                    if (english && !fiwb && lt == LangType.fry) {
                        lemmasFetcher.fetchEnglish(getOrth(f), (le) -> {
                            lemmas.add(Lemma.builder()
                                    .setForm(le)
                                    .setPos(Pos.x)
                                    .setLang(LangType.en).build());

                        });
                        
                    }
                }
                );
            }
        });
        LangType lt = LangType.valueOf(form.getLang());
        boolean fry = lt == LangType.fry;
        if (english && fry && !fiwb) {
            lemmasFetcher.fetchEnglish(getOrth(form), (le) -> {
                lemmas.add(Lemma.builder()
                        .setForm(le)
                        .setPos(Pos.x)
                        .setLang(LangType.en).build());

            });
        }
        return lemmas;
    }

    private void textFromNote(FormattedText FormattedText, org.fryske_akademy.teidictionaries.jaxb.Note note) {
        note.getContent().forEach(o -> {
            getText(FormattedText, o);
        });
    }

    private void textFromDef(List<DefContent> content, Def def) {
        def.getContent().stream().forEach(o -> {
            if (o instanceof Gloss) {
                // add gloss
                content.add(
                        org.fryske_akademy.languagemodel.Gloss.builder()
                                .setGloss(textFromGloss((Gloss) o)).build()
                );
            } else {
                // add FormattedText
                FormattedText formattedText = new FormattedText(new ArrayList<>());
                getText(formattedText, o);
                if (!formattedText.getText().isEmpty()) {
                    content.add(formattedText);
                }
            }
        });
    }

    private FormattedText textFromQuote(Quote quote) {
        FormattedText FormattedText = new FormattedText(new ArrayList<>());
        quote.getContent().forEach(o -> {
            getText(FormattedText, o);
        });
        return FormattedText;
    }

    private String getText(List objects) {
        StringBuilder sb = new StringBuilder();
        List<String> strings = new ArrayList<>(objects.size());
        objects.forEach(o -> {
            getText(sb, o);
            if (!sb.isEmpty()) {
                strings.add(sb.toString());
                sb.setLength(0);
            }
        });
        return String.join("; ", strings);
    }

    private void getText(StringBuilder sb, Object o) {
        String s = "";
        if (o instanceof Q q) {
            s = String.join("", fromContent(q.getContent(), String.class));
        } else if (o instanceof Hi h) {
            s = String.join("", fromContent(h.getContent(), String.class));
        } else if (o instanceof Gloss g) {
            s = String.join("", fromContent(g.getContent(), String.class));
        } else if (o instanceof String) {
            s = (String) o;
        }
        if (!s.isEmpty()) {
            sb.append(s);
        }
    }

    private void getText(FormattedText tl, Object o) {
        if (tl.getText() == null) {
            tl.setText(new ArrayList<>());
        }
        String s;
        if (o instanceof Q q) {
            List<TextTypeQ> content = new ArrayList<>();
            q.getContent().stream().filter(shi -> shi instanceof String || shi instanceof Hi)
                    .forEach(shi -> {
                        if (shi instanceof String ss) {
                            content.add(T.builder().setTextT(ss).build());
                        } else {
                            content.add(I.builder().setTextI(List.of(T.builder().setTextT(
                                    String.join("", fromContent(((Hi) shi).getContent(), String.class)).trim()
                            ).build())).build());
                        }
                    });
            if (!content.isEmpty()) {
                tl.getText().add(new org.fryske_akademy.languagemodel.Q(content));
            }
        } else if (o instanceof Hi h) {
            List<TextTypeI> content = new ArrayList<>();
            h.getContent().stream().filter(shi -> shi instanceof String || shi instanceof Q)
                    .forEach(shi -> {
                        if (shi instanceof String ss) {
                            content.add(T.builder().setTextT(ss).build());
                        } else {
                            content.add(org.fryske_akademy.languagemodel.Q.builder().setTextQ(List.of(T.builder().setTextT(
                                    String.join("", fromContent(((Q) shi).getContent(), String.class)).trim()
                            ).build())).build());
                        }
                    });
            if (!content.isEmpty()) {
                tl.getText().add(new org.fryske_akademy.languagemodel.I(content));
            }
        } else if (o instanceof String ss) {
            s = ss.trim();
            if (!s.isEmpty()) {
                tl.getText().add(new T(s));
            }
        } else if (isElement(o, POINTER)) {
            tl.getText().add(new L(getLink((JAXBElement<ModelPtrLike>) o, null, null, null)));
        } else if (o != null) {
            boolean skip = false;
            if (o instanceof JAXBElement j) {
                String lp = j.getName().getLocalPart();
                skip = "bibl".equals(lp);
            } else {
                skip = o instanceof Etym || o instanceof org.fryske_akademy.teidictionaries.jaxb.Note;
            }
            if (!skip) {
                LOGGER.warn(String.format("Loosing info in %s", o instanceof JAXBElement j ? "JAXB " + j.getName() : o.getClass()));
            } else if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("Loosing info in %s", o instanceof JAXBElement j ? "JAXB " + j.getName() : o.getClass()));
            }
        }
    }

    private FormattedText textFromGloss(Gloss o) {
        FormattedText formattedText = new FormattedText(new ArrayList<>());
        o.getContent().forEach(t -> getText(formattedText, t));
        return formattedText;
    }

    private String getOrth(Form form) {
        return form.getOrth()!=null?getText(form.getOrth().getContent()):"";
    }

    private Pos grammar(Form form) {
        return fromContent(form.getGrams(), Gram.class).stream().map(this::fromGram
        ).findFirst().orElse(Pos.x);
    }

    private Pos fromGram(Gram gram) {
        String v = gram.getValue().value();
        return Pos.valueOf(v.substring(v.indexOf('.')+1));
    }

    /**
     * returns true when:
     * <ul>
     * <li>gramType is null</li>
     * <li>a pos is found that equals gramType</li>
     * </ul>
     *
     * @param entry
     * @param gramType
     * @return
     */
    private boolean samePos(Entry entry, Pos gramType) {
        Pos pos = gramType == null ? null : getPos(entry);
        return gramType == null || gramType == pos;
    }
}
