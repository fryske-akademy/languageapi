package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.schema.DataFetchingEnvironment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;
import jakarta.ws.rs.NotFoundException;
import org.fryske_akademy.Util;
import org.fryske_akademy.languagemodel.Collocation;
import org.fryske_akademy.languagemodel.Example;
import org.fryske_akademy.languagemodel.FormattedText;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.LemmaLink;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.Proverb;
import org.fryske_akademy.languagemodel.SourcesFetcher;
import org.fryske_akademy.languagemodel.T;
import org.fryske_akademy.languagemodel.Text;
import org.fryske_akademy.languagemodel.TextTranslated;
import org.fryske_akademy.languagemodel.Texts;
import org.fryske_akademy.languagemodel.TextsFetcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class TextsFetcherImpl extends AbstractFetcher<Texts> implements TextsFetcher {
    public static final String SEARCHTEXT = "/searchText";
    private static final Logger LOGGER = LoggerFactory.getLogger(TextsFetcherImpl.class);
    public static final String OFFSET = "offset";
    public static final String MAX = "max";

    @Inject
    private SourcesFetcher sourcesFetcher;
    
    private HttpRequest[] textRequests(DataFetchingEnvironment environment, List<String> urls, String form) {
        return urls.stream().map(url -> headers(HttpRequest.newBuilder()
                .uri(URI.create(url + SEARCHTEXT))
                .POST(HttpRequest.BodyPublishers.ofString(
                        String.join("", "form=",
                                URLEncoder.encode(form, StandardCharsets.UTF_8),
                                "&searchlang=",
                                URLEncoder.encode(environment.getArgument(LANG), StandardCharsets.UTF_8),
                                "&offset=0&max=500",
                                sensitiveParam(environment)
                        )
                ))).build()).toList().toArray(new HttpRequest[]{});
    }

    @Override
    public Texts get(DataFetchingEnvironment environment) throws Exception {
        final int offset = environment.getArgumentOrDefault("offset", 0);
        final int max = Integer.min(environment.getArgumentOrDefault("max", 5), getPagingMax());
        final List<String> jsons = new ArrayList();
        final SearchInfo searchInfo = getSearchInfo(environment, environment.getArgument(AbstractFetcher.SEARCHTERM));
        jsons.addAll(getRequestHelper().request(getTimeoutSeconds(), textRequests(environment, searchInfo.getUrls(), searchInfo.getSearchTerms().get(0))));
        Texts.Builder builder = Texts.builder();
        if (jsons.isEmpty()) {
            return empty(String.format("%s, %s not found in dictionary text",
                    environment.getArgument(AbstractFetcher.SEARCHTERM), environment.getArgument(LANG)));
            
        } else {
            final List<Text> l = new ArrayList<>();
            for (int i = 0; i < jsons.size(); i++) {
                String json = jsons.get(i);
                String url = searchInfo.getUrls().get(i);
                if (Util.nullOrEmpty(json)) {
                    continue;
                }

                JsonValue v = Json.createReader(new StringReader(json)).readValue();
                if (!(v instanceof JsonObject)) {
                    continue;
                }

                JsonObject root = v.asJsonObject();
                JsonValue value = root.get("text");
                if (value == null) {
                    continue;
                }
                if (value.getValueType() == JsonValue.ValueType.ARRAY) {
                    JsonArray texts = (JsonArray) value;
                    texts.forEach(t -> toText(l, (JsonObject) t, getSourceId(url)));
                } else {
                    toText(l, (JsonObject) value, getSourceId(url));
                }
            }
            if (offset>=l.size()) {
                return empty(String.format("offset %d too big for total %d", offset, l.size()));
            }
            builder
                    .setTexts(l.subList(offset,l.size()))
                    .setMax(max)
                    .setOffset(offset)
                    .setTotal(l.size());
        }
        return builder.build();
    }

    private Texts empty(String s) {
        throw new NotFoundException("nothing found for " + s);
    }

    private void toText(List<Text> l, JsonObject text, String source) {
        String section = text.getString("section");
        String lemma = text.getString("lemma");
        String lang = sourcesFetcher.getSource(source).getFromlang().toString();
        String pos = text.getString("pos");
        JsonValue value = text.get("translation");
        List<TextTranslated> t = new ArrayList<>();
        if (value != null) {
            if (value.getValueType() == JsonValue.ValueType.ARRAY) {
                JsonArray translation = value.asJsonArray();
                translation.stream().map(o -> (JsonObject) o).forEach(o ->
                        addTranslation(t, o)
                );
            } else {
                JsonObject o = value.asJsonObject();
                addTranslation(t, o);
            }
        }
        if ("example".equals(section)) {
            l.add(Example.builder()
                    .setText(getFormattedText(text.getString("#text")))
                    .setLang(LangType.valueOf(text.getString("xml:lang")))
                    .setTranslations(t)
                    .setLink(buildLink(lemma, pos, lang, source))
                    .build()
            );
        } else if ("collocation".equals(section)) {
            l.add(Collocation.builder()
                    .setText(getFormattedText(text.getString("#text")))
                    .setLang(LangType.valueOf(text.getString("xml:lang")))
                    .setLink(buildLink(lemma, pos, lang, source))
                    .setTranslations(t).build()
            );
        } else if ("proverb".equals(section)) {
            l.add(Proverb.builder()
                    .setText(getFormattedText(text.getString("#text")))
                    .setLang(LangType.valueOf(text.getString("xml:lang")))
                    .setLink(buildLink(lemma, pos, lang, source))
                    .setTranslations(t).build()
            );
        }
    }

    private LemmaLink buildLink(String lemma, String pos, String lang, String source) {
        return LemmaLink.builder()
                .setLemma(lemma)
                .setSource(source)
                .setLang(LangType.valueOf(lang))
                .setPos(Pos.valueOf(pos.substring(pos.indexOf('.')+1)))
                .build();
    }

    FormattedText getFormattedText(String s) {
        FormattedText FormattedText = new FormattedText(new ArrayList<>());
        FormattedText.getText().add(new T(s));
        return FormattedText;
    }

    private void addTranslation(List<TextTranslated> t, JsonObject o) {
        t.add(TextTranslated.builder()
                .setText(getFormattedText(o.getString("#text")))
                .setLang(LangType.valueOf(o.getString("xml:lang")))
                .build());
    }
}
