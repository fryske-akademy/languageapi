package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetchingEnvironment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.NotFoundException;
import org.fryske_akademy.Util;
import org.fryske_akademy.languagemodel.GraphQLSchemaBuilder;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.TextTranslation;
import org.fryske_akademy.languagemodel.TextTranslationFetcher;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
@Named
public class TextTranslationFetcherImpl extends AbstractFetcher<TextTranslation> implements TextTranslationFetcher {

    public static final String NLFRY = "/nl2fy";
    public static final String FRYNL = "/fy2nl";

    @Inject
    @Property
    private String oersetterBaseUrl;

    @Inject
    @Property(defaultValue = "false")
    private boolean debug;

    @Inject
    private GraphQLSchemaBuilder graphQLSchemaBuilder;

    private static final String lemmaQuery = """
            query lemmasearch($searchterm: String! $lang: LangType!) {lemmasearch(searchterm: $searchterm, lang: $lang) {
                lemmas {
                  translations { form }
                }
              }
            }""";

    private static final Pattern TR = Pattern.compile("[{]form=([^}]+)[}]");
    private static final Pattern WS = Pattern.compile("\\s");

    @Override
    public TextTranslation get(DataFetchingEnvironment environment) {
        try {
            String toTranslate = environment.getArgument("text");
            LangType lang = "nl".equals(environment.getArgument(LANG)) ? LangType.nl : LangType.fry;
            if (WS.matcher(toTranslate.trim()).find()) {
                return getTextTranslation(toTranslate, lang);
            } else {
                GraphQL graphQL = GraphQL.newGraphQL(graphQLSchemaBuilder.getGraphQLSchema()).build();
                Map<String, Object> vars = new HashMap<>(2);
                vars.put(LANG, LangType.valueOf(environment.getArgument(LANG)));
                vars.put(SEARCHTERM, toTranslate);
                ExecutionInput executionInput = ExecutionInput.newExecutionInput(lemmaQuery).variables(vars).build();
                ExecutionResult result = graphQL.execute(executionInput);
                if (result.isDataPresent()) {
                    // TODO apparently no jaxb equivalent available for graphql
                    Matcher m = TR.matcher(result.toString());
                    Set<String> trs = new HashSet<>(5);
                    while (m.find()) trs.add(m.group(1));
                    if (!trs.isEmpty()) {
                        return TextTranslation.builder()
                                .setTranslationlang(lang)
                                .setTranslation(String.join(", ",trs))
                                .build();
                    } else {
                        return getTextTranslation(toTranslate,lang);
                    }
                } else {
                    return getTextTranslation(toTranslate, lang);
                }
            }
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            String s = "failed to fetch translation";
            if (e instanceof TimeoutException) s += ", timeout exceeded";
            return TextTranslation.builder()
                    .setTranslation("").build();
        }
    }

    @Override
    public TextTranslation getTextTranslation(String toTranslate, LangType lang) throws TimeoutException, ExecutionException, InterruptedException {
        String url = stripSlash(oersetterBaseUrl) + (LangType.fry==lang ? FRYNL : NLFRY);
        if (debug) {
            searchLogger.log("oersetter translate to " + lang + ": " + toTranslate);
        }
        String tr = getRequestHelper().request(
                headers(HttpRequest.newBuilder()
                        .uri(URI.create(url))
                        .POST(
                                HttpRequest.BodyPublishers.ofString(
                                        String.join("",
                                                "text=",
                                                URLEncoder.encode(toTranslate, StandardCharsets.UTF_8)
                                        )
                                )
                        ))
                        .build(), getTimeoutOersetterSeconds());
        return Util.nullOrEmpty(tr)?empty():TextTranslation.builder().setTranslation(tr).setTranslationlang(lang).build();
    }

    private TextTranslation empty() {
        throw new NotFoundException("nothing found");
    }
}
