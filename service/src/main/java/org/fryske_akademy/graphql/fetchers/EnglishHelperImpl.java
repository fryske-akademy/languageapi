/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 - 2023 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import edu.stanford.nlp.simple.Sentence;
import jakarta.enterprise.context.ApplicationScoped;
import org.fryske_akademy.languagemodel.EnglishHelper;

import java.util.List;

/**
 *
 * @author eduard
 */
@ApplicationScoped
public class EnglishHelperImpl implements EnglishHelper {
            
    @Override
    public List<String> lemmaFor(String form) {
        return new Sentence(form).lemmas();
    }
    
}
