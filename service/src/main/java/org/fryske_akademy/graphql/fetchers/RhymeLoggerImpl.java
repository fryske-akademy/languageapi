package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.fryske_akademy.languagemodel.SearchLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
@Named("rhymeLogger")
public class RhymeLoggerImpl implements SearchLogger {

    private static final Logger LOGGER = LoggerFactory.getLogger(RhymeLoggerImpl.class);

    @Inject
    @Property(defaultValue = "/rhyme.log")
    private File rhymeLog;

    @Inject
    @Property(defaultValue = "30000")
    private int searchesWriteDelay;

    @Inject
    @Property(defaultValue = "6000")
    private int searchesQueueSize;

    @Inject
    @Property(defaultValue = "true")
    private boolean test;

    private Path logPath;

    private Queue<String> searches;

    private final ScheduledExecutorService logger = Executors.newSingleThreadScheduledExecutor();

    @PostConstruct
    private void start() {
        searches = new LinkedBlockingQueue<>(searchesQueueSize);
        logPath = rhymeLog.toPath();
        logger.scheduleAtFixedRate(this::processSearches, 0, searchesWriteDelay, TimeUnit.MILLISECONDS);
    }

    private void processSearches() {
        if (searches.isEmpty()) return;
        if (test||LOGGER.isDebugEnabled())
            LOGGER.debug(String.format("%d entries in rhyme log buffer of %d max", searches.size(), searchesQueueSize));
        try (BufferedWriter out = Files.newBufferedWriter(logPath, StandardOpenOption.APPEND)) {
            while (!searches.isEmpty()) {
                String head = searches.poll();
                out.write(head + System.lineSeparator());
            }
        } catch (IOException e) {
            LOGGER.warn("failed to log rhyme", e);
        }
    }

    @Override
    public void log(String search) {
        searches.add("%s | %s".formatted(LocalDateTime.now(), search));
    }
}
