package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import graphql.schema.DataFetchingEnvironment;
import jakarta.ejb.EJBException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonString;
import jakarta.json.JsonValue;
import jakarta.ws.rs.NotFoundException;
import org.fryske_akademy.Util;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.FormTranslation;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.Lemma;
import org.fryske_akademy.languagemodel.LemmaLink;
import org.fryske_akademy.languagemodel.Lemmas;
import org.fryske_akademy.languagemodel.LemmasFetcher;
import org.fryske_akademy.languagemodel.MinLemma;
import org.fryske_akademy.languagemodel.Pos;
import org.fryske_akademy.languagemodel.SourcesFetcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.regex.Pattern;

@ApplicationScoped
public class LemmasFetcherImpl extends AbstractFetcher<Lemmas> implements LemmasFetcher {
    
    public static final String TRANSLATIONPATH = "/translate";
    private static final Logger LOGGER = LoggerFactory.getLogger(LemmasFetcherImpl.class);
    public static final Pattern LATIN = Pattern.compile("L[.]?$");
    
    @Inject
    private SourcesFetcher sourcesFetcher;
    
    @Inject
    @Property(defaultValue = "false")
    private boolean logSearch;
    
    @Inject
    @Property(defaultValue = "true")
    private boolean notFoundLogging;
    
    @Inject
    private FkwHelper fkwHelper;

    @Inject
    @Property(defaultValue = "onfw")
    private String preferredDictId;

    private HttpRequest[] lemmaRequests(LangType lt, String sensitive, List<String> urls, String form) {
        return urls.stream().map(url
                -> headers(HttpRequest.newBuilder()
                        .uri(URI.create(url + TRANSLATIONPATH))
                        .POST(HttpRequest.BodyPublishers.ofString(
                                String.join("", "form=",
                                        URLEncoder.encode(form, StandardCharsets.UTF_8),
                                        "&searchlang=",
                                        URLEncoder.encode(lt.name(), StandardCharsets.UTF_8),
                                        sensitive
                                )
                        )))
                        .build()).toList().toArray(new HttpRequest[]{});
    }
    
    private static class Lmin {
        
        private final MinLemma minLemma;
        
        public Lmin(MinLemma minLemma) {
            this.minLemma = minLemma;
        }
        
        @Override
        public boolean equals(Object o) {
            Lmin lmin = (Lmin) o;
            return minLemma.getForm().equals(lmin.minLemma.getForm())
                    && minLemma.getLink().getPos() == lmin.minLemma.getLink().getPos();
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(minLemma);
        }
    }
    
    @Override
    public Lemmas get(DataFetchingEnvironment environment) throws Exception {
        try {
            if (logSearch) {
                searchLogger.log(String.format("searchterm: %s; searchLang: %s; pos: %s",
                        environment.getArgument(SEARCHTERM),
                        environment.getArgument(LANG),
                        environment.getArgumentOrDefault("pos", "")
                ));
            }
            SearchInfo searchInfo = getSearchInfo(environment, environment.getArgument(SEARCHTERM));
            final List<MinLemma> lemmas = new ArrayList<>();
            final Set<ToSkip> toSkip = new HashSet<>();
            if (islexicon(environment)) {
                fkwLemmas(searchInfo, lemmas);
            } else {
                lemmas.addAll(getAndFilterLemmas(environment, searchInfo.getQuery(), toSkip, searchInfo.getUrls()));
                if (lemmas.isEmpty() && !searchInfo.getFkwLemmas().isEmpty()) {
                    fkwLemmas(searchInfo, lemmas);
                }
            }
            return lemmas.isEmpty() ? empty(searchInfo.getSearchTerms().get(0), environment) :
                    toLemmas(environment, lemmas, searchInfo.getSearchTerms(),searchInfo.getOrigTerm());
        } catch (IllegalArgumentException | NotFoundException e) {
            return empty(e.getMessage(), environment);
        } catch (Exception e) {
            return handleException(environment, e);
        }
    }
    
    private void fkwLemmas(SearchInfo searchInfo, final List<MinLemma> lemmas) {
        MinLemma prev = null;
        Set<Lmin> lmins = new HashSet<>();
        for (Lemma l : searchInfo.getFkwLemmas()) {
            if (prev != null && prev.getForm().equals(l.getForm()) && prev.getLink().getPos().equals(l.getPos())) {
                // skip
            } else {
                MinLemma cur = MinLemma.builder().setLang(LangType.fry)
                        .setForm(l.getForm())
                        .setLink(
                                LemmaLink.builder().setLemma(l.getForm())
                                        .setSource(SourcesFetcher.SOURCE.fkw.name())
                                        .setLang(LangType.fry)
                                        .setPos(l.getPos())
                                        .build()
                        )
                        .setPos(l.getPos())
                        .build();
                Lmin lmin = new Lmin(cur);
                if (lmins.stream().noneMatch(lm -> lm.equals(lmin))) {
                    lmins.add(lmin);
                    lemmas.add(cur);
                    prev = cur;
                }
            }
        }
    }
    
    private Lemmas handleException(DataFetchingEnvironment environment, Exception e) throws Exception {
        if (e instanceof EJBException && e.getCause() instanceof IllegalArgumentException) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("unsupported user input?", Util.deepestCause(e));
            }
            throw new IllegalArgumentException("unsupported user input?");
        }
        String s = "failed to fetch lemmas";
        if (e instanceof TimeoutException) {
            s += ", timeout exceeded";
        } else if (e instanceof IOException) {
            s += ", IO problem";
        }
        LOGGER.error(s, e);
        throw e;
    }
    
    private List<MinLemma> getAndFilterLemmas(DataFetchingEnvironment environment, String term, Set<ToSkip> toSkip, List<String> urls) throws Exception {
        HttpRequest[] requests = lemmaRequests(LangType.valueOf(environment.getArgument(LANG)), sensitiveParam(environment), urls, term);
        List<String> json = getRequestHelper().request(getTimeoutSeconds(), requests);
        List<MinLemma> lemmas = new ArrayList<>();
        short s = 0;
        /*
        TODO when searching a dictionary for lemmas in the target (to) language:
        combination lemma/pos will often be the same hence deduplicate will filter out too many links
        
        deduplication should be based on the link to details
        
        details can be found via the from language, see this example:
        {"lemma":[{"xml:lang":"fry","pos":"pos.adj","#text":"moai","tr":"fijn"},{"xml:lang":"fry","pos":"pos.adj","#text":"moai","tr":"gelegen"},{"xml:lang":"fry","pos":"pos.noun","#text":"moai","tr":"mooi"},{"xml:lang":"fry","pos":"pos.adj","#text":"moai","tr":"mooi"},{"xml:lang":"fry","pos":"pos.adj","#text":"moai","tr":"leuk"}]}        
         */
        for (String js : json) {
            if (!Util.nullOrEmpty(js)) {
                lemmas.addAll(getAndFilterLemmas(js, environment.getArgumentOrDefault("pos", ""), urls.get(s), toSkip));
            }
            s++;
        }
        return lemmas;
    }
    
    private List<MinLemma> getAndFilterLemmas(String json, String pos, String url, Set<ToSkip> toSkip) {
        List<MinLemma> minLemmas = new ArrayList<>();
        processJson(json).forEach(jo -> deduplicate(pos, url, toSkip, minLemmas, jo));
        return minLemmas;
    }
    
    private List<JsonObject> processJson(String json) {
        if (Util.nullOrEmpty(json)) {
            return Collections.emptyList();
        }
        JsonValue v = Json.createReader(new StringReader(json)).readValue();
        if (v.getValueType() == JsonValue.ValueType.STRING) {
            return Collections.emptyList();
        }
        JsonObject root = v.asJsonObject();
        JsonValue value = root.get("lemma");
        if (value.getValueType() == JsonValue.ValueType.ARRAY) {
            return List.copyOf(Set.copyOf(((JsonArray) value).getValuesAs(JsonObject.class)));
        } else {
            return List.of((JsonObject) value);
        }
        
    }
    
    private static class ToSkip {
        
        private final String posLemma, dictId;
        
        public ToSkip(String posLemma, String dictId) {
            this.posLemma = posLemma;
            this.dictId = dictId;
        }
        
        @Override
        public int hashCode() {
            int hash = 5;
            hash = 43 * hash + Objects.hashCode(this.posLemma);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof String)) {
                return false;
            }
            return posLemma.equals(obj);
        }
        
    }
    
    private void deduplicate(String pos, String url, Set<ToSkip> toSkip, List<MinLemma> minLemmas, JsonObject o) {
        // when pos argument is given skip lemmas with other pos
        if (!pos.isEmpty() && !FkwHelper.convert(o.getString("pos")).name().equals(pos)) {
            return;
        }
        LangType langType = LangType.valueOf(o.getString("xml:lang"));
        LangType sourceLang = sourcesFetcher.getSource(getSourceId(url)).getFromlang();
        JsonValue tr = o.get("tr");
        String lemma = sourceLang == langType
                ? o.getString("#text")
                : tr.getValueType() == JsonValue.ValueType.ARRAY && !tr.asJsonArray().isEmpty()
                ? ((JsonString) tr.asJsonArray().get(0)).getString()
                : ((JsonString) tr).getString();
        
        String posLemma = lemma + "|" + o.getString("pos");
        String dictId = getSourceId(url);
        Optional<ToSkip> found = toSkip.parallelStream().filter(ts -> ts.equals(posLemma)).findFirst();
        if (found.isPresent()) {
            if (dictId.equals(preferredDictId) && !found.get().dictId.equals(dictId)) {
                toSkip.remove(posLemma);
            } else {
                return;
            }
        }
        toLemma(minLemmas, o, url);
        toSkip.add(new ToSkip(posLemma, dictId));
    }
    
    private Lemmas toLemmas(DataFetchingEnvironment environment, List<MinLemma> lemmas, List<String> terms, String origTerm) {
        LangType lt = LangType.valueOf(environment.getArgument(LANG));
        final int offset = environment.getArgumentOrDefault("offset", 0);
        int max = Integer.min(environment.getArgumentOrDefault("max", 5), getPagingMax());
        int total;
        lemmas.sort(new LemmaComp(terms,origTerm).thenComparing(MinLemma::getForm));
        if (lemmas.size()>1&&lt==LangType.nl) {
            List<MinLemma> filtered = new ArrayList<>(lemmas.size());
            for (int i = 1; i < lemmas.size(); i++) {
                MinLemma prev = lemmas.get(i - 1);
                if (i == 1) filtered.add(prev);
                MinLemma curr = lemmas.get(i);
                if (!curr.getLink().getSource().equals(SourcesFetcher.SOURCE.nfwb.name())) {
                    filtered.add(curr);
                } else if (!prev.getLink().getSource().equals(SourcesFetcher.SOURCE.onfw.name())) {
                    filtered.add(curr);
                } else if (!curr.getForm().equals(prev.getForm())) {
                    filtered.add(curr);
                }
            }
            lemmas = filtered;
        }
        total = lemmas.size();
        int endIndex = offset;
        for (int i = offset; endIndex - offset < max && i < lemmas.size(); i++) {
            endIndex++;
        }
        
        // TODO move fetching english here
        lemmas = lemmas.subList(offset, endIndex);
        final boolean english = environment.getArgumentOrDefault("englishTranslations", Boolean.FALSE);
        if (english) {
            // TODO potential performance risk, fetching english for lots of results, but we do this only for one page
            lemmas.parallelStream().filter(l -> !l.getLink().getSource().equals(SourcesFetcher.SOURCE.fiwb.name())).forEach(l -> {
                if (l.getTranslations() == null) {
                    l.setTranslations(new ArrayList<>());
                }
                if (l.getLang() == LangType.fry) {
                    // TODO pos should be corrected in the dictionary
                    fetchEnglish(l.getForm(),
                            (s) -> getFormTranslation(LangType.en, s, Pos.x, false, l.getTranslations())
                    );
                }
                List<FormTranslation> fry = l.getTranslations().stream().filter(t -> t.getLang() == LangType.fry).toList();
                fry.parallelStream().forEach(tl -> fetchEnglish(tl.getForm(),
                        (s) -> getFormTranslation(LangType.en, s, Pos.x, false, l.getTranslations())
                ));
            });
        }
        return Lemmas.builder()
                .setLemmas(lemmas)
                .setOffset(offset).setMax(max).setTotal(total)
                .build();
    }
    private static final Pattern AFTER_SPACE = Pattern.compile(" .*$");
    
    private static class LemmaComp implements Comparator<MinLemma> {
        
        private final List<String> search;
        private final String origTerm;
        
        public LemmaComp(List<String> search, String origTerm) {
            this.search = search.stream().map(l
                    -> WILDCARDS.matcher(
                            AFTER_SPACE.matcher(l).replaceFirst("")
                    ).replaceAll("")
            ).toList();
            this.origTerm=WILDCARDS.matcher(
                            AFTER_SPACE.matcher(origTerm).replaceFirst("")
                    ).replaceAll("");
        }
        
        private Integer starts(String lemma) {
            if (lemma.startsWith(origTerm)) {
                return -2;
            } else if (AbstractFetcher.removeDiacrits(lemma).startsWith(search.get(0))) {
                return -1;
            } else {
                return 0;
            }
        }
        
        @Override
        public int compare(MinLemma lemma, MinLemma lemma2) {
            return starts(lemma.getForm()).compareTo(starts(lemma2.getForm()));
        }
        
    }
    
    private Lemmas empty(String s, DataFetchingEnvironment environment) {
        if (notFoundLogging && !COMPLEXTERM.matcher(s).find() && !WILDCARDS.matcher(s).find()) {
            searchLogger.log(String.format("Not found searchterm: %s; searchLang: %s; pos: %s",
                    environment.getArgument(SEARCHTERM),
                    environment.getArgument(LANG),
                    environment.getArgumentOrDefault("pos", "")
            ));
        }
        throw new NotFoundException("nothing found for " + s);
    }
    
    private void toLemma(List<MinLemma> l, JsonObject lemma, String url) {
        LangType sourceLang = sourcesFetcher.getSource(getSourceId(url)).getFromlang();
        boolean nfwb = SourcesFetcher.SOURCE.nfwb.name().equals(getSourceId(url));
        LangType langType = LangType.valueOf(lemma.getString("xml:lang"));
        // TODO here we might add english translations
        LangType trLang = langType == LangType.fry
                ? getSourceId(url).equals(SourcesFetcher.SOURCE.fiwb.name()) ? LangType.en : LangType.nl
                : LangType.fry;
        JsonValue tr = lemma.get("tr");
        String po = lemma.getString("pos");
        Pos pos = Pos.valueOf(po.substring(po.indexOf(".")+1));
        List<FormTranslation> trs = new ArrayList<>();
        if (tr != null) {
            if (tr.getValueType() == JsonValue.ValueType.ARRAY) {
                tr.asJsonArray().stream()
                        .map(t -> ((JsonString) t).getString())
                        .filter(t -> notIsLatin(langType, t))
                        .forEach(t -> getFormTranslation(trLang, t, pos, nfwb, trs));
            } else {
                String t = ((JsonString) tr).getString();
                if (notIsLatin(langType, t)) {
                    getFormTranslation(trLang, t, pos, nfwb, trs);
                }
            }
        }
        if (sourceLang != langType && trs.isEmpty()) {
            throw new IllegalArgumentException(String.format("no translation for %s, needed to show details in %s/%s", lemma.getString("#text"), getSourceId(url), sourceLang.name()));
        }
        String p = lemma.getString("pos");
        l.add(MinLemma.builder()
                .setForm(lemma.getString("#text"))
                .setPos(Pos.valueOf(p.substring(p.indexOf('.')+1)))
                .setLink(
                        LemmaLink.builder()
                                .setSource(getSourceId(url))
                                .setPos(Pos.valueOf(p.substring(p.indexOf('.')+1)))
                                .setLemma(
                                        sourceLang == langType ? lemma.getString("#text")
                                                : trs.get(0).getForm()
                                ).setLang(sourceLang)
                                .build()
                )
                .setTranslations(trs.isEmpty() ? null : trs)
                .setLang(langType)
                .build());
    }
    
    @Override
    public void fetchEnglish(String lemma, Consumer<String> c) {
        String enUrl = getSourceUrl(SourcesFetcher.SOURCE.fiwb.name());
        HttpRequest h = lemmaRequests(LangType.fry, "", List.of(enUrl), lemma)[0];
        // fetch english translations
        try {
            String response = getRequestHelper().request(h, getTimeoutSeconds());
            processJson(response).forEach(jo -> {
                JsonValue etr = jo.get("tr");
                if (etr != null) {
                    if (etr.getValueType() == JsonValue.ValueType.ARRAY) {
                        etr.asJsonArray()
                                .forEach(t -> c.accept(((JsonString) t).getString()));
//                                        -> trs.add(getFormTranslation(LangType.en, ((JsonString) t).getString(), pos, false)));
                    } else {
                        c.accept(((JsonString) etr).getString());
//                        trs.add(getFormTranslation(LangType.en, ((JsonString) etr).getString(), pos, false));
                    }
                }
                
            });
            
        } catch (TimeoutException | ExecutionException | InterruptedException e) {
            LOGGER.warn("unable to fetch english", e);
        }
    }
    
    private boolean notIsLatin(LangType langType, String t) {
        if (langType == LangType.nl && LATIN.matcher(t).find()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Skipping latin: " + t);
            }
            return false;
        }
        return true;
    }
    
    private void getFormTranslation(LangType langType, String t, Pos pos, boolean nfwb, List<FormTranslation> l) {
        l.add(FormTranslation.builder().setForm(
                TeiToDetailsMapperImpl.replaceCapital(
                        fkwHelper,
                        langType == LangType.fry,
                        pos,
                        t, nfwb)
        ).setLang(langType).build());
    }
    
}
