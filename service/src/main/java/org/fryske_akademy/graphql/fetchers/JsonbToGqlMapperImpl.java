package org.fryske_akademy.graphql.fetchers;

/*-
 * #%L
 * languageservice
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm;
import org.fa.foarkarswurdlist.jpa.jsonb.Paradigm;
import org.fryske_akademy.languagemodel.FkwHelper;
import org.fryske_akademy.languagemodel.JsonbToGqlMapper;
import org.fryske_akademy.languagemodel.LangType;
import org.fryske_akademy.languagemodel.Lemma;

import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class JsonbToGqlMapperImpl implements JsonbToGqlMapper {

    @Inject
    private FkwHelper fkwHelper;

    @Override
    public Lemma map(org.fa.foarkarswurdlist.jpa.jsonb.Lemma lemma, boolean synonyms, boolean variants, boolean dutchisms) {
        Lemma l = Lemma.builder()
                .setLang(LangType.fry)
                .setForm(lemma.getForm())
                .setMeaning(lemma.getMeaning())
                .setArticle(lemma.getArticle())
                .setHyphenation(
                        lemma.getParadigms().stream().filter(p -> lemma.getForm().equals(p.getForm()) && p.getHyphenation() != null)
                                .map(Paradigm::getHyphenation)
                                .findFirst().orElse(null)
                )
                .setPronunciation(
                        lemma.getParadigms().stream().filter(p -> lemma.getForm().equals(p.getForm()) && p.getIpa() != null)
                                .map(Paradigm::getIpa)
                                .findFirst().orElse(null)
                )
                .setSubForms(new ArrayList<>())
                .setPos(
                        FkwHelper.convert(lemma.getPos())
                )
                .build();
        LemmaParadigm lp = new LemmaParadigm();
        lp.setArticle(lemma.getArticle());
        lp.setForm(lemma.getForm());
        lp.setPos(lemma.getPos());
        lp.setParadigms(lemma.getParadigms());
        fkwHelper.getFormInfos(List.of(lp)).forEach(fi -> l.getSubForms().add(fi));
        if (variants) l.getSubForms().addAll(fkwHelper.getVariants(lemma.getForm(), FkwHelper.convert(lemma.getPos())));
        if (synonyms)
            l.getSubForms().addAll(lemma.getSynonyms().stream().map(FkwHelper::mapSynonym).toList());
        if (dutchisms)
            l.getSubForms().addAll(lemma.getDutchisms().stream().map(FkwHelper::mapDutchism).toList());
        return l;
    }
}
