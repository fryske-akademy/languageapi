package org.fryske_akademy.languageapi.servlet;

/*-
 * #%L
 * languageservlet
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.kickstart.execution.GraphQLObjectMapper;
import graphql.kickstart.servlet.GraphQLConfiguration;
import graphql.kickstart.servlet.GraphQLHttpServlet;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.fryske_akademy.languagemodel.DetailsFetcher;
import org.fryske_akademy.languagemodel.GraphQLSchemaBuilder;
import org.fryske_akademy.languagemodel.GraphqlSimpleWiring;
import org.fryske_akademy.languagemodel.IndexFetcher;
import org.fryske_akademy.languagemodel.LemmasFetcher;
import org.fryske_akademy.languagemodel.RhymeFetcher;
import org.fryske_akademy.languagemodel.SourcesFetcher;
import org.fryske_akademy.languagemodel.TextTranslationFetcher;
import org.fryske_akademy.languagemodel.TextsFetcher;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@WebServlet(name = "GraphQLLexiconServlet", urlPatterns = {"/graphql/*"}, loadOnStartup = 1)
public class GraphQLServlet extends GraphQLHttpServlet {

    @Inject
    private SourcesFetcher sourcesFetcher;

    @Inject
    private IndexFetcher indexFetcher;

    @Inject
    private LemmasFetcher lemmasFetcher;

    @Inject
    private DetailsFetcher detailsFetcher;

    @Inject
    private TextTranslationFetcher textTranslationFetcher;

    @Inject
    private TextsFetcher textsFetcher;

    @Inject
    private RhymeFetcher rhymeFetcher;

    @Inject
    private GraphQLSchemaBuilder graphQLSchemaBuilder;

    @Inject
    private ErrorListener errorListener;

    @Inject
    private DatafetchingErrorHandler datafetchingErrorHandler;

    private static GraphQLConfiguration configuration = null;

    @PostConstruct
    private void initConfig() {
        if (configuration == null) {
            createSchema();
            GraphQLConfiguration.Builder builder = GraphQLConfiguration
                    .with(graphQLSchemaBuilder.getGraphQLSchema())
                    .with(List.of(errorListener))
                    .with(GraphQLObjectMapper.newBuilder().withGraphQLErrorHandler(datafetchingErrorHandler).build());
            configuration = builder.build();
        }
    }

    @Override
    protected GraphQLConfiguration getConfiguration() {
        return configuration;
    }


    private void createSchema() {
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder
                        .dataFetcher(LemmasFetcher.LEMMASEARCH, lemmasFetcher)
                        .dataFetcher(IndexFetcher.INDEX, indexFetcher)
                        .dataFetcher(TextsFetcher.TEXTSEARCH, textsFetcher)
                        .dataFetcher(DetailsFetcher.DETAILS, detailsFetcher)
                        .dataFetcher(RhymeFetcher.RHYMEFETCHER, rhymeFetcher)
                        .dataFetcher(TextTranslationFetcher.TRANSLATETEXT, textTranslationFetcher)
                        .dataFetcher(SourcesFetcher.SOURCES, sourcesFetcher)
                )
                .wiringFactory(GraphqlSimpleWiring.GRAPHQL_SIMPLE_WIRING)
                .build();
        graphQLSchemaBuilder.setGraphQLSchema(
                new SchemaGenerator().makeExecutableSchema(graphQLSchemaBuilder.getTypeDefinitionRegistry(), runtimeWiring)
        );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.setCharacterEncoding(StandardCharsets.UTF_8.name());
            resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
        super.doPost(req, resp);
    }
}
